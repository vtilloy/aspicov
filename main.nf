#!/usr/bin/env nextflow
/*
========================================================================================
                                    ASPICov workflow                                     
========================================================================================
 Pipeline to identify Sars-Cov2 virus sequences from NGS sequencing data.
 #### Homepage / Documentation
https://gitlab.com/vtilloy/aspicov
----------------------------------------------------------------------------------------
*/


nextflow.enable.dsl=2

def helpMessage() {
    // Add to this help message with new command line parameters
    log.info SeBiMER_CHUHeader()
    log.info"""

    Usage:

    The typical command for running the pipeline after filling the conf/custom.config file is as follows:

	nextflow run main.nf -profile custom,singularity
	""".stripIndent()
}


// Show help message
if (params.help) {
    helpMessage()
    exit 0
}


/*
 * SET UP CONFIGURATION VARIABLES
 */

// Has the run name been specified by the user?
//  this has the bonus effect of catching both -name and --name
custom_runName = params.name
if (!(workflow.runName ==~ /[a-z]+_[a-z]+/)) {
    custom_runName = workflow.runName
}

params.date = new java.util.Date()

//Copy config files to output directory for each run
paramsfile = file("$baseDir/conf/base.config", checkIfExists: true)
paramsfile.copyTo("$params.outdir/${params.projectName}/00_pipeline_conf/base.config")

if (workflow.profile.contains('test')) {
   testparamsfile = file("$baseDir/conf/test.config", checkIfExists: true)
   testparamsfile.copyTo("$params.outdir/${params.projectName}/00_pipeline_conf/test.config")
}
if (workflow.profile.contains('custom.config')) {
   customparamsfile = file("$baseDir/conf/custom.config", checkIfExists: true)
   customparamsfile.copyTo("$params.outdir/${params.projectName}/00_pipeline_conf/custom.config")
}
if (workflow.profile.contains('custom1')) {
   customparamsfile = file("$baseDir/conf/custom1.config", checkIfExists: true)
   customparamsfile.copyTo("$params.outdir/${params.projectName}/00_pipeline_conf/custom1.config")
}

/*
 * PIPELINE INFO
 */

// Header log info
log.info SeBiMER_CHUHeader()
def summary = [:]
if (workflow.revision) summary['Pipeline Release'] = workflow.revision
summary['Run Name'] = custom_runName ?: workflow.runName
summary['Project Name'] = params.projectName
summary['Output dir'] = params.outdir
summary['Launch dir'] = workflow.launchDir
summary['Working dir'] = workflow.workDir
summary['Script dir'] = workflow.projectDir
summary['User'] = workflow.userName
summary['Profile'] = workflow.profile
if (params.technology == "Illumina") {     
    summary['Technology Type'] = "Illumina"
} else {
    summary['Technology Type'] = "Iontorrent"
}
if (params.method == "Ampliseq") {
    summary['Method'] = "Ampliseq"
    summary['Input Files'] = params.bedpe_file
} else {      
    summary['Method'] = "Capture"
}

if (params.technology == "Illumina") {
    summary['Input Files'] = params.reads
} else {    
    summary['Input Files'] = params.bam
}
summary['Reference Genome'] = params.ref_fasta

log.info summary.collect { k,v -> "${k.padRight(18)}: $v" }.join("\n")
log.info "-\033[91m--------------------------------------------------\033[0m-"

// Check the hostnames against configured profiles
checkHostname()



/*
 * VERIFY AND SET UP WORKFLOW VARIABLES
 */



if (!params.technology ==~ ('Illumina' || 'Iontorrent')) {
    log.error "No valid technology has been provided. Please configure the 'technology' parameter in the custom.config file to either 'Illumina' or 'Iontorrent'"
    exit 1
}


if (!params.method ==~ ('Ampliseq' || 'Capture')) {
    log.error "No valid approach has been provided. Please configure the 'method' parameter in the custom.config file to either 'Ampliseq' or 'Capture'"
    exit 1
}


if (params.technology == 'Illumina' && params.reads.isEmpty()) {
    log.error "No valid path for data location is provided. Please configure the 'reads' parameter in the custom.config file."
    exit 1
}


if (params.technology == 'Iontorrent' && params.bam.isEmpty()) {
    log.error "No valid path for data location is provided. Please configure the 'bam' parameter in the custom.config file."
    exit 1
}

if (params.ref_bed.isEmpty()) {
    log.error "No valid path for reference genome (.bed) location is provided. Please configure the 'ref_bed' parameter in the custom.config file."
    exit 1
}

if (params.ref_genome.isEmpty()) {
    log.error "No valid path for reference genome (.genome) location is provided. Please configure the 'ref_genome' parameter in the custom.config file."
    exit 1
}

if (params.ref_fasta.isEmpty()) {
    log.error "No valid path for reference genome (.fasta) location is provided. Please configure the 'ref_fasta' parameter in the custom.config file."
    exit 1
}


if (params.trimmomatic_adapter_file.isEmpty()) {
    log.error "No valid path for trimmomatic adapter file location is provided. Please configure the 'trimmomatic_adapter_file' parameter in the custom.config file."
    exit 1
}

if (params.bedpe_file.isEmpty()) {
    log.error "No valid path for bedpe file location is provided. Please configure the 'bedpe_file' parameter in the custom.config file."
    exit 1
}

if (workflow.profile.contains('custom')) {

if (params.technology == 'Illumina') {
   Channel
       .fromFilePairs(params.reads)
       .ifEmpty { error "Cannot find any reads matching: ${params.reads}" }
       .set{ read_pairs_ch }
}

if (params.technology == 'Iontorrent') {
   Channel
       .fromPath(params.bam)
       .ifEmpty { error "Cannot find any bam matching: ${params.bam}" }
       .map { file -> tuple(file.baseName, file) }
       .set{ bam_file_ch }
}

}

include { trimmomatic } from './modules/trimmomatic.nf'
include { fastqc } from './modules/fastqc.nf'
include { bwa;  bwa2 } from './modules/bwa.nf'
include { samtools; samtools2 } from './modules/samtools.nf'
include { synthesis1; synthesis2; synthesis3 } from './modules/synthesis.nf'
include { primer_trimming } from './modules/primer_trimming.nf'
include { igvtools } from './modules/igvtools.nf'
include { cvplot } from './modules/cvplot.nf'
include { freebayes } from './modules/freebayes.nf'
include { normalization } from './modules/normalization.nf'
include { vcfallelicprimitives } from './modules/vcfallelicprimitives.nf'
include { snpeff } from './modules/snpeff.nf'
include { bcftools } from './modules/bcftools.nf'
include { filter } from './modules/filter.nf'
include { snpsift; bcftools_extr } from './modules/snpsift.nf'
include { pvariant } from './modules/pvariant.nf'
include { bgzip; tabix; consensus; extraction } from './modules/consensus.nf'
include { svh } from './modules/svh.nf'
include { bamtools } from './modules/bamtools.nf'
include { prinseq } from './modules/prinseq.nf'
include { get_test_data } from './modules/get_test_data.nf'
include { index } from './modules/index.nf'
include { pangolin } from './modules/pangolin.nf'
include { denovo } from './modules/megahit.nf'
include { quast } from './modules/quast.nf'
include { get_software_version; get_input_info; report_multiqc_config; html_report } from './modules/multiqc.nf'


/*
 * RUN MAIN WORKFLOW
 */

workflow {

    main:

ref_fasta = Channel.fromPath(params.ref_fasta)
ref_genome = Channel.fromPath(params.ref_genome)
ref_bed = Channel.fromPath(params.ref_bed)
sam_ch = Channel.empty()
bam_ch = Channel.empty()
depth_ch = Channel.empty()
stats_ch = Channel.empty()
bcftools_extr_ch = Channel.empty()
consensus_ch = Channel.empty()


    if (workflow.profile.contains('test')) {
      get_test_data()

      read_pairs_ch = get_test_data.out.read_pairs

      get_test_data.out.bam_file.flatten().map{file -> tuple(file.baseName, file)}.set{bam_file_ch}
 
      ref_genome = get_test_data.out.ref_genome
      ref_fasta = get_test_data.out.ref_fasta
      ref_bed = get_test_data.out.ref_bed
      }


    if (params.technology == 'Illumina') {
      trimmomatic(read_pairs_ch)
      fastqc(trimmomatic.out.trimmed_files)
      bwa(trimmomatic.out.trimmed_files)
      denovo(trimmomatic.out.trimmed_files)
    }
   
    if (params.technology == 'Iontorrent') { 
      bamtools(bam_file_ch)
      prinseq(bamtools.out.fastq_file)
      fastqc(prinseq.out.trimmed_files)
      bwa(prinseq.out.trimmed_files)
      denovo(prinseq.out.trimmed_files)
    }

      index(ref_fasta)

    if (params.method == 'Ampliseq') {
      primer_trimming(bwa.out.sam_file)
      bwa2(primer_trimming.out.primerclipped_file)
      sam_ch = bwa2.out.sam2_file
      samtools2(sam_ch)
      bam_ch = samtools2.out.bam2_file
      depth_ch = samtools2.out.depth_file.collect()
      stats_ch = samtools2.out.stats_file.collect()
    }

    if (params.method == 'Capture') {
      sam_ch = bwa.out.sam_file
      samtools(sam_ch)
      bam_ch = samtools.out.bam_file
      depth_ch = samtools.out.depth_file.collect()
      stats_ch = samtools.out.stats_file.collect()
    }

      synthesis1(depth_ch)
      synthesis2(stats_ch)

      igvtools(bam_ch)
      cvplot(igvtools.out.wig_file)
      freebayes(bam_ch)

      normalization(freebayes.out.vcf_tmp_file)
      vcfallelicprimitives(normalization.out.vcf_norm_file)
      snpeff(vcfallelicprimitives.out.vcf_file)
      bcftools(snpeff.out.vcf_annot_file)
      filter(bcftools.out.vcf_annot_filter_file)
      pvariant(filter.out.vcf_annot_filter_pass_file_to_plot.collect())
      snpsift(filter.out.vcf_annot_filter_pass_file)
      bcftools_extr_ch = filter.out.vcf_annot_filter_pass_file.join(snpsift.out.xls_fields_file)
      bcftools_extr(bcftools_extr_ch)
      synthesis3(filter.out.variant_files.collect())
      bgzip(filter.out.vcf_annot_filter_pass_file)
      tabix(bgzip.out.vcf_zip_file)
      consensus_ch = bgzip.out.vcf_zip_file.join(tabix.out.vcf_index)
      consensus(consensus_ch)
      extraction(consensus.out.consensus_file_nn) 
      pangolin(extraction.out.consensus_file.collect())
      svh(bcftools_extr.out.vcf_simple_file.collect())
      quast(denovo.out.denovo_file)    
      get_software_version()
      get_input_info()
      report_multiqc_config()
      html_report(
            get_software_version.out.software_versions_yaml.toList(),
            report_multiqc_config.out.multiQCconfig_file, 
            fastqc.out.fastqc_file.collect(),
//            samtools.out.samtools_stats_file.collect(),
            stats_ch,
            bcftools.out.bcftools_stats_file.collect(),
            snpeff.out.snpeff_stats_file.collect(),
            pangolin.out.genotyping_file.collect(),
            quast.out.quast_stats_file.collect(),            
            get_input_info.out.input_files.collect(),
            )

}


/*
 * Completion notification
 */

workflow.onComplete {
    c_green = params.monochrome_logs ? '' : "\033[0;32m";
    c_purple = params.monochrome_logs ? '' : "\033[0;35m";
    c_red = params.monochrome_logs ? '' : "\033[0;31m";
    c_reset = params.monochrome_logs ? '' : "\033[0m";

    if (workflow.success) {
        log.info "-${c_purple}[Covid analysis workflow]${c_green} Pipeline completed successfully${c_reset}-"
    } else {
        checkHostname()
        log.info "-${c_purple}[Covid analysis workflow]${c_red} Pipeline completed with errors${c_reset}-"
    }
}






/*
 * Other functions
 */


def SeBiMER_CHUHeader() {
    // Log colors ANSI codes
    c_red = params.monochrome_logs ? '' : "\033[0;91m";
    c_blue = params.monochrome_logs ? '' : "\033[1;94m";
    c_reset = params.monochrome_logs ? '' : "\033[0m";
    c_yellow = params.monochrome_logs ? '' : "\033[1;93m";
    c_Ipurple = params.monochrome_logs ? '' : "\033[0;95m" ;

    return """    -${c_red}---------------------------------------------------------------------------------${c_reset}-
    ${c_blue}    __  __  __  .       __  __        __                         __   __   __  __     ${c_reset}
    ${c_blue}   \\   |_  |__) | |\\/| |_  |__)   /  |   |__| |  |   |   | |\\/| |  | |  _ |_   \\       ${c_reset}
    ${c_blue}  __\\  |__ |__) | |  | |__ |  \\  /   |__ |  | |__|   |__ | |  | |__| |__| |__ __\\      ${c_reset}
                                                                                                                 ${c_reset}
    ${c_yellow}  ASPICov workflow (version ${workflow.manifest.version})${c_reset}
                                            ${c_reset}
    ${c_Ipurple}  Homepage: ${workflow.manifest.homePage}${c_reset}
    -${c_red}---------------------------------------------------------------------------------${c_reset}-
    """.stripIndent()
}

def checkHostname() {
    def c_reset = params.monochrome_logs ? '' : "\033[0m"
    def c_white = params.monochrome_logs ? '' : "\033[0;37m"
    def c_red = params.monochrome_logs ? '' : "\033[1;91m"
    def c_yellow_bold = params.monochrome_logs ? '' : "\033[1;93m"
    if (params.hostnames) {
        def hostname = "hostname".execute().text.trim()
        params.hostnames.each { prof, hnames ->
            hnames.each { hname ->
                if (hostname.contains(hname) && !workflow.profile.contains(prof)) {
                    log.error "====================================================\n" +
                            "  ${c_red}WARNING!${c_reset} You are running with `-profile $workflow.profile`\n" +
                            "  but your machine hostname is ${c_white}'$hostname'${c_reset}\n" +
                            "  ${c_yellow_bold}It's highly recommended that you use `-profile $prof${c_reset}`\n" +
                            "============================================================"
                }
            }
        }
    }
}

