#!/usr/bin/python

# Purpose of the script: bed2bedpe for bamclipper from primer bed file

with open("artic_v4-1_ncov-2019-primer.bed", "r") as infile:
    data = infile.readlines()

primers = {}
for line in data:
    fields = line.strip().split("\t")
    primer = fields[3]
    if primer.endswith("_LEFT"):
        chrom = fields[0]
        start = fields[1]
        end = fields[2]
        if primer[:-5] not in primers:
            primers[primer[:-5]] = {"left": {"chrom": chrom, "start": start, "end": end}, "right": None}
        else:
            primers[primer[:-5]]["left"] = {"chrom": chrom, "start": start, "end": end}
    elif primer.endswith("_RIGHT"):
        chrom = fields[0]
        start = fields[1]
        end = fields[2]
        if primer[:-6] not in primers:
            primers[primer[:-6]] = {"left": None, "right": {"chrom": chrom, "start": start, "end": end}}
        else:
            primers[primer[:-6]]["right"] = {"chrom": chrom, "start": start, "end": end}

with open("output.bedpe", "w") as outfile:
    for key, value in primers.items():
        if value["left"] and value["right"]:
            outfile.write("\t".join([value["left"]["chrom"], value["left"]["start"], value["left"]["end"], value["right"]["chrom"], value["right"]["start"], value["right"]["end"]]) + "\n")
