# **ASPICov: Automated and Standardized Pipeline for Identification of SARS-Cov2 nucleotidic Variants**.

[![ASPICov version](https://img.shields.io/badge/ASPICov%20version-1.2.0-red)](https://www.nextflow.io/)
[![Nextflow](https://img.shields.io/badge/nextflow-%E2%89%A521.04.3.5563-23aa62.svg)](https://www.nextflow.io/)
[![Run with with singularity](https://img.shields.io/badge/run%20with-singularity-1d355c.svg)](https://sylabs.io/docs/)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Cite Publication](https://img.shields.io/badge/Cite%20Us!-Cite%20Publication-purple)](https://pubmed.ncbi.nlm.nih.gov/35081137/) 

 ## Introduction

The ASPICov pipeline is built using [Nextflow](https://www.nextflow.io) a workflow tool to run tasks across multiple compute infrastructures in a very portable manner. It comes with Singularity containers making installation trivial and results highly reproducible.

## Quick Start

i. Install [`nextflow`](https://www.nextflow.io/docs/latest/getstarted.html#installation)

ii. Install [`Singularity`](https://www.sylabs.io/guides/3.0/user-guide/) for full pipeline reproducibility

iii. Download the pipeline and test it on a minimal dataset with a single command

```bash
nextflow run main.nf -profile test,singularity
```

> To use this workflow on a computing cluster, it is necessary to provide a configuration file for your system. For some institutes, this one already exists and is referenced on [nf-core/configs](https://github.com/nf-core/configs#documentation). If so, you can simply download your institute custom config file and simply use `-c <institute_config_file>` in your command. This will enable `singularity` and set the appropriate execution settings for your local compute environment.

iv. Start running your own analysis!

```bash
nextflow run main.nf -profile custom,singularity [-c <institute_config_file>]
```

See [usage docs](docs/usage.md) for a complete description of all of the options available when running the pipeline.

## Documentation

This workflow comes with documentation about the pipeline, found in the `docs/` directory:

1. [Introduction](docs/usage.md#introduction)
2. [Pipeline installation](docs/usage.md#install-the-pipeline)
    * [Local installation](docs/usage.md#local-installation)
    * [Adding your own system config](docs/usage.md#your-own-config)
3. [Running the pipeline](docs/usage.md#running-the-pipeline)
4. [Output and how to interpret the results](docs/output.md)
5. [Troubleshooting](docs/troubleshooting.md)

Here is an overview of the many steps available in ASPICov pipeline:

![ASPICov](docs/Images/ASPICov_workflow.png )


## Requirements

To use ASPICov, all tools are automatically installed via pre-built singularity images available at [SeBiMER ftp](https://data-dataref.ifremer.fr/bioinfo/ifremer/sebimer/tools/AspiCov/); these images are built from recipes available [here](/containers). 

## Credits

ASPICov is a workflow developed by [Valentin Tilloy](https://gitlab.com/vtilloy) (CHU Limoges: Centre National de Référence des Herpèsvirus / UF9481 Bioinformatique) and ported to Nextflow by [Pierre Cuzin](https://gitlab.com/Tyabe) ([SeBiMER](https://ifremer-bioinformatics.github.io/), the Bioinformatics Core Facility of [IFREMER](https://wwz.ifremer.fr/en/).)

Software developed as part of the French National [Obepine](https://www.reseau-obepine.fr/) Network.

## Citation

Valentin Tilloy, Pierre Cuzin, Laura Leroi, Emilie Guérin, Patrick Durand, Sophie Alain

ASPICov: An automated pipeline for identification of SARS-Cov2 nucleotidic variants

PLoS One 2022 Jan 26;17(1):e0262953. [PMID:35081137](https://pubmed.ncbi.nlm.nih.gov/35081137/)

## Contributions

We welcome contributions to the pipeline. If such case you can do one of the following:
* Use issues to submit your questions 
* Fork the project, do your developments and submit a pull request
* Contact us (see email below) 

## Support

For further information or help, don't hesitate to get in touch with the ASPICov developpers: 

**valentin.tilloy@unilim.fr**

![sebimer email](assets/sebimer-email.png)
