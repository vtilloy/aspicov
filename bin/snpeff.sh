#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Variant annotation                                     ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
VCFFILE=${args[0]}
PREFIX=${args[1]}
LOGCMD=${args[2]}


snpEff="java -jar /usr/local/share/snpeff-4.5covid19-2/snpEff.jar"
# Run Snpeff
    CMD="$snpEff -v NC_045512.2 ${VCFFILE} > ${PREFIX}-annot.vcf"
    echo $CMD > $LOGCMD
    eval $CMD

# Stats for report
    CMD="$snpEff -v NC_045512.2 ${VCFFILE} -csvStats ${PREFIX}-annot.csv"
    echo $CMD >> $LOGCMD
    eval $CMD
