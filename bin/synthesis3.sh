#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: files synthesis 3                                      ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
FILTERSDIRECTORY=${args[0]}
LOGCMD=${args[1]}

#synthesis
    CMD="cat ${FILTERSDIRECTORY}/*.total.txt > all_total.txt;
	 cat ${FILTERSDIRECTORY}/*.stop.txt > all_stop.txt;
	 cat ${FILTERSDIRECTORY}/*.miss.txt > all_miss.txt"
    echo $CMD > $LOGCMD
    eval $CMD