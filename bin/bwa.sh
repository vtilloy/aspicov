#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Bwa mapping                                            ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
REF=${args[0]}
LOGCMD=${args[1]}
TRIMMEDREADS=${args[2]}
PREFIX=${args[3]}


# Run Bwa
#    CMD="bwa index ${REF}"
#    echo $CMD > $LOGCMD
#    eval $CMD

    CMD="bwa mem ${REF} ${TRIMMEDREADS} > ${PREFIX}_trimmed.sam"
    echo $CMD >> $LOGCMD
    eval $CMD
