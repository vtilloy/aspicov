#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Retreive software versions                             ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
FILE=${args[0]}
LOGCMD=${args[1]}

# Retreive version number from nextflow.config into single version txt files
awk '/container/' ${FILE} > versions-v.txt
cat versions-v.txt |awk -F"/" '{print $3}' > interm
sed -i 's+-+/+g' interm
sed 's://.*$::' interm > interm1
sed -i 's/"$//;s/:/\//g' interm1
awk -F"/" '{ fname=$1 "-v.txt"; print > fname; close(fname) }' interm1

# Python to integrate version values into yaml
CMD="get_versions.py > software_versions_mqc.yaml"
echo $CMD >> $LOGCMD
eval $CMD

