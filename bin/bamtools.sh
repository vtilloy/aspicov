#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Generating fastq from ubam                             ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
BAMFILE=${args[0]}
PREFIX=${args[1]}
LOGCMD=${args[2]}


# Run Bamtools
    CMD="bamtools convert -in ${BAMFILE} -format fastq > ${PREFIX}.fastq"
    echo $CMD > $LOGCMD
    eval $CMD
