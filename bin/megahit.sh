#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: De novo assembly                                       ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
TECH=${args[0]}
PREFIX=${args[1]}
LOGCMD=${args[2]}

if [ $TECH == 'Illumina' ]
then
    TRIMMEDFILE_F=${args[3]}
    TRIMMEDFILE_R=${args[4]}

elif [ $TECH == 'Iontorrent' ]
then
    TRIMMEDFILE=${args[3]}
fi

# Run Megahit
if [ $TECH == 'Illumina' ]
then
    CMD="megahit -1 ${TRIMMEDFILE_F} -2 ${TRIMMEDFILE_R} -o ${PREFIX}-megahit --out-prefix ${PREFIX} --min-contig-len 500"
    echo $CMD > $LOGCMD
    eval $CMD
elif [ $TECH == 'Iontorrent' ]
then
    CMD="megahit -r ${TRIMMEDFILE} -o ${PREFIX}-megahit --out-prefix ${PREFIX} --min-contig-len 500"
    echo $CMD >> $LOGCMD
    eval $CMD
fi


# Get file even if no contigs
    CMD="touch -a ${PREFIX}-megahit/${PREFIX}.contigs.fa"
    echo $CMD >> $LOGCMD
    eval $CMD
     
    CMD="cp ${PREFIX}-megahit/${PREFIX}.contigs.fa ${PREFIX}.contigs.fasta"
    echo $CMD >> $LOGCMD
    eval $CMD