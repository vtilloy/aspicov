#!/usr/bin/env python3

import sys
import shutil
import tempfile
from Bio import SeqIO

primer_file = sys.argv[1]
sam_file_path = sys.argv[2]
fasta_file_path = sys.argv[3]
name = sys.argv[4]

primers = []
with open(primer_file, 'r') as bedpe_file:
    for line in bedpe_file:
        fields = line.strip().split('\t')
        primer_start = int(fields[1])
        primer_end = int(fields[2])
        primers.append((primer_start, primer_end))

reads = []
trimmed_reads = []
total_reads = 0

with open(sam_file_path, 'r') as sam_file:
    header_lines = []
    for line in sam_file:
        if line.startswith('@'):
            header_lines.append(line)
            continue
        fields = line.strip().split('\t')
        read_name = fields[0]
        flag = fields[1]
        reference_name = fields[2]
        position = fields[3]
        mapping_quality = fields[4]
        cigar = fields[5]
        mate_reference_name = fields[6]
        mate_position = fields[7]
        insert_size = fields[8]
        read_sequence = fields[9]
        quality_scores = fields[10]

        total_reads += 1

        trimmed_sequence = read_sequence
        trimmed_quality = quality_scores

        for primer_start, primer_end in primers:
            if read_sequence.startswith(reference_name[primer_start:primer_end]) and primer_start == 0:
                trimmed_sequence = read_sequence[primer_end:]
                trimmed_quality = quality_scores[primer_end - primer_start:]
                trimmed_reads.append((read_name, trimmed_sequence, trimmed_quality))
                break
            elif read_sequence.endswith(reference_name[primer_start:primer_end]) and primer_end == len(read_sequence):
                trimmed_sequence = read_sequence[:primer_start]
                trimmed_quality = quality_scores[:len(read_sequence) - primer_start]
                trimmed_reads.append((read_name, trimmed_sequence, trimmed_quality))
                break

        reads.append((read_name, read_sequence, quality_scores))

output_fastq_path = f'{name}_clipped.fastq'
output_fasta_path = f'{name}_trimmed_sequences.fasta'

with open(output_fastq_path, 'w') as fastq_file, open(output_fasta_path, 'w') as fasta_file:
    for read_name, read_sequence, quality_scores in reads:
        fastq_file.write(f'@{read_name}\n{read_sequence}\n+\n{quality_scores}\n')
        fasta_file.write(f'>{read_name}\n{read_sequence}\n')

    for read_name, trimmed_sequence, trimmed_quality in trimmed_reads:
        fastq_file.write(f'@{read_name}_trimmed\n{trimmed_sequence}\n+\n{trimmed_quality}\n')

output_stats_path = f'{name}_trimstats.txt'

with open(output_stats_path, 'w') as stats_file:
    stats_file.write(f'number of total reads = {total_reads}\n')
    stats_file.write(f'number of bedpe trimmed reads = {len(trimmed_reads)}\n')