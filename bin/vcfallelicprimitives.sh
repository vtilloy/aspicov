#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: VCF normalization                                      ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
VCFFILE=${args[0]}
PREFIX=${args[1]}
LOGCMD=${args[2]}


#vcfallelicprimitives="vcflib vcfallelicprimitives"
# Run vcfallelicprimitives
    CMD="vcfallelicprimitives -k ${VCFFILE} > ${PREFIX}.vcf"
    echo $CMD > $LOGCMD
    eval $CMD