#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Consensus                                              ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
VCFFILE=${args[0]}
PREFIX=${args[1]}
REF=${args[2]}
LOGCMD=${args[3]}

# Run bcftools
    CMD="cat ${REF} | bcftools consensus ${VCFFILE} > ${PREFIX}-nn-consensus.fasta"
    echo $CMD > $LOGCMD
    eval $CMD
