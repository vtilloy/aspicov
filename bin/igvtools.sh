#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Mean depth                                             ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
PREFIX=${args[0]}
BAMFILE=${args[1]}
GENOME=${args[2]}
LOGCMD=${args[3]}


# Run igvtools
    CMD="igvtools count -w 1 --bases ${BAMFILE} ${PREFIX}.wig ${GENOME}"
    echo $CMD > $LOGCMD
    eval $CMD
