#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Simple VCF                                             ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
VCFPASS=${args[0]}
PREFIX=${args[1]}
LOGCMD=${args[2]}


SnpSift="java -jar /usr/local/share/snpsift-5.1d-0/SnpSift.jar"

#simple vcf
   CMD="$SnpSift extractFields -s ',' -e "EMPTY" ${VCFPASS} "CHROM" "POS" "REF" "ALT" "FILTER" "DP" "ANN[0].GENE" "ANN[*].HGVS_P" > ${PREFIX}_fields.xls"
   echo $CMD > $LOGCMD
   eval $CMD