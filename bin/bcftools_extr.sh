#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Simple VCF                                             ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
VCFPASS=${args[0]}
PREFIX=${args[1]}
XLSFIELDS=${args[2]}
LOGCMD=${args[3]}


#simple vcf including VAF
   CMD="bcftools query -f '%CHROM %POS %REF %ALT %DP[ %VAF]\n' ${VCFPASS} > ${PREFIX}-VAF.xls"
   echo $CMD > $LOGCMD
   eval $CMD

   CMD="sed -i '1iCHROM\tPOS\tREF\tALT\tDP\tVAF' ${PREFIX}-VAF.xls"
   echo $CMD >> $LOGCMD
   eval $CMD

#file junction
paste <(awk '{print $1, $2, $3, $4, $5, $6}' ${PREFIX}-VAF.xls) <(awk '{print $7, $8, $9}' ${XLSFIELDS}) > ${PREFIX}_pass_simple_tmp.xls

#cleaning
   CMD="sed -e 's/\<MODIFIER\>//g' -e 's/\<EMPTY\>//g' ${PREFIX}_pass_simple_tmp.xls  > ${PREFIX}_pass_simple-tmp.xls"
   echo $CMD >> $LOGCMD
   eval $CMD
   CMD="sed -i '1s/.*/reference,genome_position,REF,ALT,depth,allelic_frequency,gene,amino_acid_mutation_(if_ORF)/' ${PREFIX}_pass_simple-tmp.xls"
   echo $CMD >> $LOGCMD
   eval $CMD
# convert comma into tabulation
   CMD="tr -s ',' <${PREFIX}_pass_simple-tmp.xls | tr ',' '\t' >${PREFIX}_tmp"
   echo $CMD >> $LOGCMD
   eval $CMD
# convert whitespace into tabulation
awk -v OFS="\t" '$1=$1' ${PREFIX}_tmp > ${PREFIX}_pass_simple.xls