#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: denovo qc                                              ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
DENOVOFILE=${args[0]}
PREFIX=${args[1]}
LOGCMD=${args[2]}


quast="python /usr/local/bin/quast"
# Run Quast
    CMD="$quast ${DENOVOFILE} -o ${PREFIX}-quast"
    echo $CMD > $LOGCMD
    eval $CMD


# Get files even if no contigs
    CMD="touch -a ${PREFIX}-quast/report.pdf"
    echo $CMD >> $LOGCMD
    eval $CMD
     
    CMD="touch -a ${PREFIX}-quast/report.tsv"
    echo $CMD >> $LOGCMD
    eval $CMD
     
    CMD="cp ${PREFIX}-quast/report.pdf ${PREFIX}-report.pdf"
    echo $CMD >> $LOGCMD
    eval $CMD
     
    CMD="cp ${PREFIX}-quast/report.tsv ${PREFIX}-report.tsv"
    echo $CMD >> $LOGCMD
    eval $CMD
     




