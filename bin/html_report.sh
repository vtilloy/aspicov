#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: HTML summary Reports                                   ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
TITLE=${args[0]}
LOGCMD=${args[1]}

# Run Multiqc for global report
#export LD_LIBRARY_PATH=/usr/lib:/usr/local/lib:$LD_LIBRARY_PATH
    CMD="multiqc -f -n ${TITLE}_pipeline_report ."
    echo $CMD > $LOGCMD
    eval $CMD
