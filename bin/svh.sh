#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: highlight variants of concern (VOC)                    ##
##                          and variants of interest (VOI)                   ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
VCF=${args[0]}
LOGCMD=${args[1]}
PIPELINE_START_DATE=`date +\%Y-\%m-\%d-\%Hh\%Mm\%Ss`
LOGFILE2=detection-variants-"${PIPELINE_START_DATE}.report.xls"


# Run Special variant highlight
    touch ${LOGFILE2}
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1
echo "Highlight: variants of concern (VOC) and variants of interest (VOI)"  >> ${LOGFILE2} 2>&1
echo "ASPICov" >> ${LOGFILE2} 2>&1
echo "updated = november 07 2024" >> ${LOGFILE2} 2>&1
echo "from who.int, covariants.org, cov-lineages.org and cov-spectrum.org" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1
echo ""  >> ${LOGFILE2} 2>&1
echo "pipeline execution date : ${PIPELINE_START_DATE}" >> ${LOGFILE2} 2>&1
echo ""  >> ${LOGFILE2} 2>&1

echo "analysed files :" >> ${LOGFILE2} 2>&1
for a in ${VCF}/*pass_simple.xls;
do
echo "$a"                                                                         >> ${LOGFILE2} 2>&1
done
echo ""                                                                           >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


###

for a in ${VCF}/*pass_simple.xls;
do
grep -F -e "reference" -e "p.His69_Val70del" -e "p.Tyr144del" -e "p.Asn501Tyr" -e "p.Ala570Asp" -e "p.Asp614Gly" -e "p.Pro681His" -e "p.Thr716Ile" -e "p.Ser982Ala" -e "p.Asp1118His" $a > $a-variant-anglais 
done

echo "Detection of previously circulating alpha VOC (NextStrain 20I / PANGO B.1.1.7). Exhaustive mutations on S protein: Δ69-70del, Δ144, N501Y, A570D, D614G, P681H, T716I, S982A, D1118H" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


for e in ${VCF}/*variant-anglais;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;



echo ""  >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-anglais;


###
###

for a in ${VCF}/*pass_simple.xls;
do
grep -F -e "reference" -e "p.Leu18Phe" -e "p.Asp80Ala" -e "p.Asp215Gly" -e "p.Leu241_Leu242_Ala243del" -e "p.Lys417Thr" -e "p.Glu484Lys" -e "p.Asn501Tyr" -e "p.Asp614Gly" -e "p.Ala701Val" $a > $a-variant-sud-africain
done

echo "Detection of previously circulating beta VOC (NextStrain 20H / PANGO B.1.351). Exhaustive mutations on S protein: L18F, D80A, D215G, Δ241-242-243del, K417N, E484K, N501Y, D614G, A701V" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1

for e in ${VCF}/*variant-sud-africain;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo "" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-sud-africain;


###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Leu18Phe" -e "p.Thr20Asn" -e "p.Pro26Ser" -e "p.Asp138Tyr" -e "p.Arg190Ser" -e "p.Lys417Thr" -e "p.Glu484Lys" -e "p.Asn501Tyr" -e "p.Asp614Gly" -e "p.His655Tyr" -e "p.Thr1027Ile" -e "p.Val1176Phe" $a > $a-variant-bresilien
done

echo "Detection of previously circulating gamma VOC (NextStrain 20J / PANGO P.1). Exhaustive mutations on S protein: L18F, T20N, P26S, D138Y, R190S, K417N, E484K, N501Y, D614G, H655Y, T1027I, V1176F" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-bresilien ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-bresilien;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Thr19Arg" -e "p.Gly142Asp" -e "p.Glu156_Phe157del" -e "p.Arg158Gly" -e "p.Leu452Arg" -e "p.Thr478Lys" -e "p.Asp614Gly" -e "p.Pro681Arg" -e "p.Asp950Asn" $a > $a-variant-indien
done

echo "Detection of previously circulating delta VOC (NextStrain 21A / PANGO B.1.617.2). Exhaustive mutations on S protein: T19R, G142D, Δ156-157, R158G, L452R, T478K, D614G, P681R, D950N" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-indien ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-indien;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Ala67Val" -e "p.His69_Val70del" -e "p.Thr95Ile" -e "p.Gly142_Val143_Tyr144del" -e "p.Tyr145Asp" -e "p.Asn211del" -e "p.Leu212Ile" -e "p.Gly339Asp" -e "p.Ser371Leu" -e "p.Ser373Pro" -e "p.Ser375Phe" -e "p.Lys417Asn" -e "p.Asn440Lys" -e "p.Gly446Ser" -e "p.Ser477Asn" -e "p.Thr478Lys" -e "p.Glu484Ala" -e "p.Gln493Arg" -e "p.Gly496Ser" -e "p.Gln498Arg" -e "p.Asn501Tyr" -e "p.Tyr505His" -e "p.Thr547Lys" -e "p.Asp614Gly" -e "p.His655Tyr" -e "p.Asn679Lys" -e "p.Pro681His" -e "p.Asn764Lys" -e "p.Asp796Tyr" -e "p.Asn856Lys" -e "p.Gln954His" -e "p.Asn969Lys" -e "p.Leu981Phe" $a > $a-variant-omicron-ba1
done

echo "Detection of currently circulating omicron VOC - BA.1 sublineage (NextStrain 21K / PANGO BA.1). Exhaustive mutations on S protein: A67V, Δ69-70, T95I, Δ142-144, Y145D, Δ211, L212I, G339D, S371L, S373P, S375F, K417N, N440K, G446S, S477N, T478K, E484A, Q493R, G496S Q498R, N501Y, Y505H, D614G, H655Y, N679K, P681H, N764K, D796Y, N856K, Q954H, N969K, L981F" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-omicron-ba1 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-omicron-ba1;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Thr19Ile" -e "p.Leu24del" -e "p.Pro25del" -e "p.Pro26del" -e "p.Ala27Ser" -e "p.Gly142Asp" -e "p.Val213Gly" -e "p.Gly339Asp" -e "p.Ser371Phe" -e "p.Ser373Pro" -e "p.Ser375Phe" -e "p.Thr376Ala" -e "p.Asp405Asn" -e "p.Arg408Ser" -e "p.Lys417Asn" -e "p.Asn440Lys" -e "p.Ser477Asn" -e "p.Thr478Lys" -e "p.Glu484Ala" -e "p.Gln493Arg" -e "p.Gln498Arg" -e "p.Asn501Tyr" -e "p.Tyr505His" -e "p.Asp614Gly" -e "p.His655Tyr" -e "p.Asn679Lys" -e "p.Pro681His" -e "p.Asn764Lys" -e "p.Asp796Tyr" -e "p.Gln954His" -e "p.Asn969Lys"  -e "p.Ser704Leu" $a > $a-variant-omicron-ba2
done

echo "Detection of currently circulating omicron VOC - BA.2 sublineage (NextStrain 21L / PANGO BA.2). Exhaustive mutations on S protein: T19I, Δ24, Δ25, Δ26, A27S, G142D, V213G, G339D, S371F, S373P, S375F, T376A, D405N, R408S, K417N, N440K, S477N, T478K, E484A, Q493R, Q498R, N501Y, Y505H, D614G, H655Y, N679K, P681H, N764K, D796Y, Q954H, N969K" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-omicron-ba2 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-omicron-ba2;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference"  -e "p.Ala67Val" -e "p.His69_Val70del" -e "p.Thr95Ile" -e "p.Gly142Asp" -e "p.Val143_Tyr144_Tyr145del" -e "p.Asn211Ile" -e "p.Leu212del" -e "p.Gly339Asp" -e "p.Ser371Leu" -e "p.Ser373Pro" -e "p.Ser375Phe" -e "p.Asp405Asn" -e "p.Asn440Lys" -e "p.Gly446Ser" -e "p.Ser477Asn" -e "p.Thr478Lys" -e "p.Glu484Ala" -e "p.Gln493Arg" -e "p.Gln498Arg" -e "p.Asn501Tyr" -e "p.Tyr505His" -e "p.Asp614Gly" -e "p.His655Tyr" -e "p.Asn679Lys" -e "p.Pro681His" -e "p.Asn764Lys" -e "p.Asp796Tyr" -e "p.Gln954His" -e "p.Asn969Lys" $a > $a-variant-omicron-ba3
done

echo "Detection of omicron VOC - BA.3 sublineage (NextStrain 21M / PANGO BA.3). Exhaustive mutations on S protein: A67V, Δ69-70, T95I, G142D, Δ143-145, N211I, Δ212, G339D, S371L, S373P, S375F, D405N, N440K, G446S, S477N, T478K, E484A, Q493R, Q498R, N501Y, Y505H, D614G, H655Y, N679K, P681H, N764K, D796Y, Q954H, N969K" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-omicron-ba3 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-omicron-ba3;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Thr19Ile" -e "p.Leu24del" -e "p.Pro25del" -e "p.Pro26del" -e "p.Ala27Ser" -e "p.His69_Val70del" -e "p.Gly142Asp" -e "p.Val213Gly" -e "p.Gly339Asp" -e "p.Ser371Phe" -e "p.Ser373Pro" -e "p.Ser375Phe" -e "p.Thr376Ala" -e "p.Asp405Asn" -e "p.Arg408Ser" -e "p.Lys417Asn" -e "p.Asn440Lys" -e "p.Leu452Arg" -e "p.Ser477Asn" -e "p.Thr478Lys" -e "p.Glu484Ala" -e "p.Phe486Val" -e "p.Gln498Arg" -e "p.Asn501Tyr" -e "p.Tyr505His" -e "p.Asp614Gly" -e "p.His655Tyr" -e "p.Asn679Lys" -e "p.Pro681His" -e "p.Asn764Lys" -e "p.Asp796Tyr" -e "p.Gln954His" -e "p.Asn969Lys"  -e "p.Ser704Leu" $a > $a-variant-omicron-ba4-5
done

echo "Detection of currently circulating omicron VOC - BA.4 and BA.5 sublineages (NextStrain 22A-22B / PANGO BA.4_BA-5). Exhaustive mutations on S protein: T19I, Δ24, Δ25, Δ26, A27S, Δ69-70, G142D, V213G, G339D, S371F, S373P, S375F, T376A, D405N, R408S, K417N, N440K, L452R, S477N, T478K, E484A, F486V, Q498R, N501Y, Y505H, D614G, H655Y, N679K, P681H, N764K, D796Y, Q954H, N969K on S protein" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-omicron-ba4-5 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-omicron-ba4-5;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Thr19Ile" -e "p.Leu24del" -e "p.Pro25del" -e "p.Pro26del" -e "p.Ala27Ser" -e "p.Gly142Asp" -e "p.Val213Gly" -e "p.Gly339Asp" -e "p.Ser371Phe" -e "p.Ser373Pro" -e "p.Ser375Phe" -e "p.Thr376Ala" -e "p.Asp405Asn" -e "p.Arg408Ser" -e "p.Lys417Asn" -e "p.Asn440Lys" -e "p.Leu452Gln" -e "p.Ser477Asn" -e "p.Thr478Lys" -e "p.Glu484Ala" -e "p.Gln493Arg" -e "p.Gln498Arg" -e "p.Asn501Tyr" -e "p.Tyr505His" -e "p.Asp614Gly" -e "p.His655Tyr" -e "p.Asn679Lys" -e "p.Pro681His" -e "p.Ser704Leu" -e "p.Asn764Lys" -e "p.Asp796Tyr" -e "p.Gln954His" -e "p.Asn969Lys"  -e "p.Ser704Leu" $a > $a-variant-omicron-BA.2.12.1
done

echo "Detection of currently circulating omicron VOC - BA.2.12.1 sublineage (NextStrain 22C / PANGO BA.2.12.1). Exhaustive mutations on S protein: T19I, Δ24, Δ25, Δ26, A27S, G142D, V213G, G339D, S371F, S373P, S375F, T376A, D405N, R408S, K417N, N440K, L452Q, S477N, T478K, E484A, Q493R, Q498R, N501Y, Y505H, D614G, H655Y, N679K, P681H, S704L, N764K, D796Y, Q954H, N969K on S protein" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1

for e in ${VCF}/*variant-omicron-BA.2.12.1 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-omicron-BA.2.12.1;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Thr19Ile" -e "p.Leu24del" -e "p.Pro25del" -e "p.Pro26del"  -e "p.Ala27Ser" -e "p.Gly142Asp" -e "p.Lys147Glu" -e "p.Trp152Arg" -e "p.Phe157Leu" -e "p.Ile210Val" -e "p.Val213Gly" -e "p.Gly257Ser" -e "p.Gly339Asp" -e "p.Ser371Phe" -e "p.Ser373Pro" -e "p.Ser375Phe" -e "p.Thr376Ala" -e "p.Asp405Asn" -e "p.Arg408Ser" -e "p.Lys417Asn" -e "p.Asn440Lys" -e "p.Gly446Ser" -e "p.Asn460Lys" -e "p.Ser477Asn" -e "p.Thr478Lys" -e "p.Glu484Ala" -e "p.Gln493Arg" -e "p.Gln498Arg" -e "p.Asn501Tyr" -e "p.Tyr505His" -e "p.Asp614Gly" -e "p.His655Tyr" -e "p.Asn679Lys" -e "p.Pro681His" -e "p.Asn764Lys" -e "p.Asp796Tyr" -e "p.Gln954His" -e "p.Asn969Lys" $a > $a-variant-omicron-BA.2.75
done

echo "Detection of currently circulating omicron VOC - BA.2.75 sublineage (NextStrain 22D / PANGO BA.2.75). Exhaustive mutations on S protein: T19I, Δ24, Δ25, Δ26, A27S, G142D, K147E, W152R, F157L, I210V, V213G, G257S, G339D, S371F, S373P, S375F, T376A, D405N, R408S, K417N, N440K, G446S, N460K, S477N, T478K, E484A, Q493R, Q498R, N501Y, Y505H, D614G, H655Y, N679K, P681H, N764K, D796Y, Q954H, N969K" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-omicron-BA.2.75 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-omicron-BA.2.75;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Thr19Ile" -e "p.Leu24del" -e "p.Pro25del" -e "p.Pro26del" -e "p.Ala27Ser" -e "p.His69_Val70del" -e "p.Gly142Asp" -e "p.Val213Gly" -e "p.Gly339Asp" -e "p.Ser371Phe" -e "p.Ser373Pro" -e "p.Ser375Phe" -e "p.Thr376Ala" -e "p.Asp405Asn" -e "p.Arg408Ser" -e "p.Lys417Asn" -e "p.Asn440Lys" -e "p.Lys444Thr" -e "p.Leu452Arg" -e "p.Asn460Lys" -e "p.Ser477Asn" -e "p.Thr478Lys" -e "p.Glu484Ala" -e "p.Phe486Val" -e "p.Gln493Gln" -e "p.Gln498Arg" -e "p.Asn501Tyr" -e "p.Tyr505His" -e "p.Asp614Gly" -e "p.His655Tyr" -e "p.Asn679Lys" -e "p.Pro681His" -e "p.Asn764Lys" -e "p.Asp796Tyr" -e "p.Gln954His" -e "p.Asn969Lys" $a > $a-variant-omicron-BQ.1
done

echo "Detection of currently circulating omicron VOC - BQ.1 sublineage (NextStrain 22E / PANGO BQ.1). Exhaustive mutations on S protein: T19I, Δ24, Δ25, Δ26, A27S, Δ69-70, G142D, V213G, G339D, S371F, S373P, S375F, T376A, D405N, R408S, K417N, N440K, K444T, L452R, N460K, S477N, T478K, E484A, F486V, Q493Q, Q498R, N501Y, Y505H, D614G, H655Y, N679K, P681H, N764K, D796Y, Q954H, N969K" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-omicron-BQ.1 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-omicron-BQ.1;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Thr19Ile" -e "p.Leu24del" -e "p.Pro25del" -e "p.Pro26del" -e "p.Ala27Ser" -e "p.Val83Ala" -e "p.Gly142Asp" -e "p.Tyr144del" -e "p.His146Gln" -e "p.Gln183Glu" -e "p.Val213Glu" -e "p.Gly339His" -e "p.Arg346Thr" -e "p.Leu368Ile" -e "p.Ser371Phe" -e "p.Ser373Pro" -e "p.Ser375Phe" -e "p.Thr376Ala" -e "p.Asp405Asn" -e "p.Arg408Ser" -e "p.Lys417Asn" -e "p.Asn440Lys" -e "p.Lys444Thr" -e "p.Val445Pro" -e "p.Gly446Ser" -e "p.Asn460Lys" -e "p.Ser477Asn" -e "p.Thr478Lys" -e "p.Glu484Ala" -e "p.Phe486Ser" -e "p.Phe490Ser" -e "p.Arg493Gln" -e "p.Gln498Arg" -e "p.Asn501Tyr" -e "p.Tyr505His" -e "p.Asp614Gly" -e "p.His655Tyr" -e "p.Asn679Lys" -e "p.Pro681His" -e "p.Asn764Lys" -e "p.Asp796Tyr" -e "p.Gln954His" -e "p.Asn969Lys" $a > $a-variant-omicron-XBB
done

echo "Detection of currently circulating omicron VOC - XBB sublineage (NextStrain 22F / PANGO XBB). Exhaustive mutations on S protein: T19I, Δ24, Δ25, Δ26, A27S, V83A, G142D, Δ144, H146Q, Q183E, V213E, G339H, R346T, L368I, S371F, S373P, S375F, T376A, D405N, R408S, K417N, N440K, K444T, V445P, G446S, N460K, S477N, T478K, E484A, F486S, F490S, R493Q, Q498R, N501Y, Y505H, D614G, H655Y, N679K, P681H, N764K, D796Y, Q954H, N969K" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-omicron-XBB ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-omicron-XBB;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Thr19Ile" -e "p.Leu24del" -e "p.Pro25del" -e "p.Pro26del" -e "p.Ala27Ser" -e "p.His69_Val70del" -e "p.Gly142Asp" -e "p.Val213Gly" -e "p.Gly339Asp" -e "p.Arg346Thr" -e "p.Ser371Phe" -e "p.Ser373Pro" -e "p.Ser375Phe" -e "p.Thr376Ala" -e "p.Asp405Asn" -e "p.Arg408Ser" -e "p.Lys417Asn" -e "p.Asn440Lys" -e "p.Leu452Arg" -e "p.Ser477Asn" -e "p.Thr478Lys" -e "p.Glu484Ala" -e "p.Phe486Val" -e "p.Gln498Arg" -e "p.Asn501Tyr" -e "p.Tyr505His" -e "p.Asp614Gly" -e "p.His655Tyr" -e "p.Asn679Lys" -e "p.Pro681His" -e "p.Asn764Lys" -e "p.Asp796Tyr" -e "p.Gln954His" -e "p.Asn969Lys"  -e "p.Ser704Leu" $a > $a-variant-omicron-BF7
done

echo "Detection of currently circulating omicron VOC - BF7 sublineage (PANGO BA.5.2.1.7). Exhaustive mutations on S protein: T19I, Δ24, Δ25, Δ26, A27S, Δ69-70, G142D, V213G, G339D, R346T, S371F, S373P, S375F, T376A, D405N, R408S, K417N, N440K, L452R, S477N, T478K, E484A, F486V, Q498R, N501Y, Y505H, D614G, H655Y, N679K, P681H, N764K, D796Y, Q954H, N969K on S protein" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-omicron-BF7 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-omicron-BF7;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Thr19Ile" -e "p.Leu24del" -e "p.Pro25del" -e "p.Pro26del" -e "p.Ala27Ser" -e "p.Val83Ala" -e "p.Gly142Asp" -e "p.Tyr144del" -e "p.His146Gln" -e "p.Gln183Glu" -e "p.Val213Glu" -e "p.Gly252Val" -e "p.Gly339His" -e "p.Arg346Thr" -e "p.Leu368Ile" -e "p.Ser371Phe" -e "p.Ser373Pro" -e "p.Ser375Phe" -e "p.Thr376Ala" -e "p.Asp405Asn" -e "p.Arg408Ser" -e "p.Lys417Asn" -e "p.Asn440Lys" -e "p.Val445Pro" -e "p.Gly446Ser" -e "p.Asn460Lys" -e "p.Ser477Asn" -e "p.Thr478Lys" -e "p.Glu484Ala" -e "p.Phe486Pro" -e "p.Phe490Ser" -e "p.Arg493Gln" -e "p.Gln498Arg" -e "p.Asn501Tyr" -e "p.Tyr505His" -e "p.Asp614Gly" -e "p.His655Tyr" -e "p.Asn679Lys" -e "p.Pro681His" -e "p.Asn764Lys" -e "p.Asp796Tyr" -e "p.Gln954His" -e "p.Asn969Lys" $a > $a-variant-omicron-XBB.1.5
done

echo "Detection of currently circulating omicron VOC - XBB.1.5 sublineage (NextStrain 23A / PANGO XBB.1.5). Exhaustive mutations on S protein: T19I, Δ24, Δ25, Δ26, A27S, V83A, G142D, Δ144, H146Q, Q183E, V213E, G252V, G339H, R346T, L368I, S371F, S373P, S375F, T376A, D405N, R408S, K417N, N440K, V445P, G446S, N460K, S477N, T478K, E484A, F486P, F490S, R493Q, Q498R, N501Y, Y505H, D614G, H655Y, N679K, P681H, N764K, D796Y, Q954H, N969K" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-omicron-XBB.1.5 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-omicron-XBB.1.5;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Thr19Ile" -e "p.Leu24del" -e "p.Pro25del" -e "p.Pro26del" -e "p.Ala27Ser" -e "p.Val83Ala" -e "p.Gly142Asp" -e "p.Tyr144del" -e "p.His146Gln" -e "p.Glu180Val" -e "p.Gln183Glu" -e "p.Val213Glu" -e "p.Gly252Val" -e "p.Gly339His" -e "p.Arg346Thr" -e "p.Leu368Ile" -e "p.Ser371Phe" -e "p.Ser373Pro" -e "p.Ser375Phe" -e "p.Thr376Ala" -e "p.Asp405Asn" -e "p.Arg408Ser" -e "p.Lys417Asn" -e "p.Asn440Lys" -e "p.Val445Pro" -e "p.Gly446Ser" -e "p.Asn460Lys" -e "p.Ser477Asn" -e "p.Thr478Arg" -e "p.Glu484Ala" -e "p.Phe486Pro" -e "p.Phe490Ser" -e "p.Arg493Gln" -e "p.Gln498Arg" -e "p.Asn501Tyr" -e "p.Tyr505His" -e "p.Asp614Gly" -e "p.His655Tyr" -e "p.Asn679Lys" -e "p.Pro681His" -e "p.Asn764Lys" -e "p.Asp796Tyr" -e "p.Gln954His" -e "p.Asn969Lys" $a > $a-variant-omicron-XBB.1.16
done

echo "Detection of currently circulating omicron VOC - XBB.1.16 sublineage (NextStrain 23B / PANGO XBB.1.16). Exhaustive mutations on S protein: T19I, Δ24, Δ25, Δ26, A27S, V83A, G142D, Δ144, H146Q, E180V, Q183E, V213E, G252V, G339H, R346T, L368I, S371F, S373P, S375F, T376A, D405N, R408S, K417N, N440K, V445P, G446S, N460K, S477N, T478R, E484A, F486P, F490S, R493Q, Q498R, N501Y, Y505H, D614G, H655Y, N679K, P681H, N764K, D796Y, Q954H, N969K" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-omicron-XBB.1.16 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-omicron-XBB.1.16;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Thr19Ile" -e "p.Leu24del" -e "p.Pro25del" -e "p.Pro26del" -e "p.Ala27Ser" -e "p.Gly142Asp" -e "p.Lys147Glu" -e "p.Trp152Arg" -e "p.Phe157Leu" -e "p.Ile210Val" -e "p.Val213Gly" -e "p.Gly257Ser" -e "p.Gly339His" -e "p.Arg346Thr" -e "p.Ser371Phe" -e "p.Ser373Pro" -e "p.Ser375Phe" -e "p.Thr376Ala" -e "p.Asp405Asn" -e "p.Arg408Ser" -e "p.Lys417Asn" -e "p.Asn440Lys" -e "p.Lys444Thr" -e "p.Gly446Ser" -e "p.Leu452Arg" -e "p.Asn460Lys" -e "p.Ser477Asn" -e "p.Thr478Lys" -e "p.Glu484Ala" -e "p.Phe486Ser" -e "p.Arg493Gln" -e "p.Gln498Arg" -e "p.Asn501Tyr" -e "p.Tyr505His" -e "p.Asp614Gly" -e "p.His655Tyr" -e "p.Asn679Lys" -e "p.Pro681His" -e "p.Asn764Lys" -e "p.Asp796Tyr" -e "p.Gln954His" -e "p.Asn969Lys" $a > $a-variant-omicron-CH.1.1
done

echo "Detection of currently circulating omicron VOC - CH.1.1 sublineage (NextStrain 23C / PANGO CH.1.1). Exhaustive mutations on S protein: T19I, Δ24, Δ25, Δ26, A27S, G142D, K147E, W152R, F157L, I210V, V213G, G257S, G339H, R346T, S371F, S373P, S375F, T376A, D405N, R408S, K417N, N440K, K444T, G446S, L452R, N460K, S477N, T478K, E484A, F486S, R493Q, Q498R, N501Y, Y505H, D614G, H655Y, N679K, P681H, N764K, D796Y, Q954H, N969K" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-omicron-CH.1.1 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-omicron-CH.1.1;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Thr19Ile" -e "p.Leu24del" -e "p.Pro25del" -e "p.Pro26del" -e "p.Ala27Ser" -e "p.Val83Ala" -e "p.Gly142Asp" -e "p.Tyr144del" -e "p.His146Gln" -e "p.Gln183Glu" -e "p.Val213Glu" -e "p.Gly252Val" -e "p.Gly339His" -e "p.Arg346Thr" -e "p.Leu368Ile" -e "p.Ser371Phe" -e "p.Ser373Pro" -e "p.Ser375Phe" -e "p.Thr376Ala" -e "p.Asp405Asn" -e "p.Arg408Ser" -e "p.Lys417Asn" -e "p.Asn440Lys" -e "p.Val445Pro" -e "p.Gly446Ser" -e "p.Asn460Lys" -e "p.Ser477Asn" -e "p.Thr478Lys" -e "p.Glu484Ala" -e "p.Phe486Ser" -e "p.Phe490Ser" -e "p.Arg493Gln" -e "p.Gln498Arg" -e "p.Asn501Tyr" -e "p.Tyr505His" -e "p.Asp614Gly" -e "p.His655Tyr" -e "p.Asn679Lys" -e "p.Pro681His" -e "p.Asn764Lys" -e "p.Asp796Tyr" -e "p.Gln954His" -e "p.Asn969Lys" $a > $a-variant-omicron-XBB.1.9
done

echo "Detection of currently circulating omicron VOC - XBB.1.9 sublineage (NextStrain 23D / PANGO XBB.1.9). Exhaustive mutations on S protein: T19I, Δ24, Δ25, Δ26, A27S, V83A, G142D, Δ144, H146Q, Q183E, V213E, G252V, G339H, R346T, L368I, S371F, S373P, S375F, T376A, D405N, R408S, K417N, N440K, V445P, G446S, N460K, S477N, T478K, E484A, F486S, F490S, R493Q, Q498R, N501Y, Y505H, D614G, H655Y, N679K, P681H, N764K, D796Y, Q954H, N969K" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-omicron-XBB.1.9 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-omicron-XBB.1.9;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Thr19Ile" -e "p.Leu24del" -e "p.Pro25del" -e "p.Pro26del" -e "p.Ala27Ser" -e "p.Val83Ala" -e "p.Gly142Asp" -e "p.Tyr144del" -e "p.His146Gln" -e "p.Gln183Glu" -e "p.Val213Glu" -e "p.Asp253Gly" -e "p.Gly339His" -e "p.Arg346Thr" -e "p.Leu368Ile" -e "p.Ser371Phe" -e "p.Ser373Pro" -e "p.Ser375Phe" -e "p.Thr376Ala" -e "p.Asp405Asn" -e "p.Arg408Ser" -e "p.Lys417Asn" -e "p.Asn440Lys" -e "p.Val445Pro" -e "p.Gly446Ser" -e "p.Asn460Lys" -e "p.Ser477Asn" -e "p.Thr478Lys" -e "p.Glu484Ala" -e "p.Phe486Pro" -e "p.Phe490Ser" -e "p.Arg493Gln" -e "p.Gln498Arg" -e "p.Asn501Tyr" -e "p.Tyr505His" -e "p.Pro521Ser" -e "p.Asp614Gly" -e "p.His655Tyr" -e "p.Asn679Lys" -e "p.Pro681His" -e "p.Asn764Lys" -e "p.Asp796Tyr" $a > $a-variant-omicron-XBB.2.3
done

echo "Detection of currently circulating omicron VOC - XBB.2.3 sublineage (NextStrain 23E / PANGO XBB.2.3). Exhaustive mutations on S protein: T19I, Δ24, Δ25, Δ26, A27S, V83A, G142D, Δ144, H146Q, Q183E, V213E, D253G, G339H, R346T, L368I, S371F, S373P, S375F, T376A, D405N, R408S, K417N, N440K, V445P, G446S, N460K, S477N, T478K, E484A, F486P, F490S, R493Q, Q498R, N501Y, Y505H, P521S, D614G, H655Y, N679K, P681H, N764K, D796Y" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-omicron-XBB.2.3 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-omicron-XBB.2.3;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Thr19Ile" -e "p.Leu24del" -e "p.Pro25del" -e "p.Pro26del" -e "p.Ala27Ser" -e "p.Gln52His" -e "p.Val83Ala" -e "p.Gly142Asp" -e "p.Tyr144del" -e "p.His146Gln" -e "p.Gln183Glu" -e "p.Val213Glu" -e "p.Gly252Val" -e "p.Gly339His" -e "p.Arg346Thr" -e "p.Leu368Ile" -e "p.Ser371Phe" -e "p.Ser373Pro" -e "p.Ser375Phe" -e "p.Thr376Ala" -e "p.Asp405Asn" -e "p.Arg408Ser" -e "p.Lys417Asn" -e "p.Asn440Lys" -e "p.Val445Pro" -e "p.Gly446Ser" -e "p.Phe456Leu" -e "p.Asn460Lys" -e "p.Ser477Asn" -e "p.Thr478Lys" -e "p.Glu484Ala" -e "p.Phe486Pro" -e "p.Phe490Ser" -e "p.Arg493Gln" -e "p.Gln498Arg" -e "p.Asn501Tyr" -e "p.Tyr505His" -e "p.Asp614Gly" -e "p.His655Tyr" -e "p.Asn679Lys" -e "p.Pro681His" -e "p.Asn764Lys" -e "p.Asp796Tyr" -e "p.Gln954His" -e "p.Asn969Lys" $a > $a-variant-omicron-EG.5.1
done

echo "Detection of currently circulating omicron VOC - EG.5.1 sublineage (NextStrain 23F / PANGO EG.5.1). Exhaustive mutations on S protein: T19I, Δ24, Δ25, Δ26, A27S, Q52H, V83A, G142D, Δ144, H146Q, Q183E, V213E, G252V, G339H, R346T, L368I, S371F, S373P, S375F, T376A, D405N, R408S, K417N, N440K, V445P, G446S, F456L, N460K, S477N, T478K, E484A, F486P, F490S, R493Q, Q498R, N501Y, Y505H, D614G, H655Y, N679K, P681H, N764K, D796Y, Q954H, N969K" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-omicron-EG.5.1 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-omicron-EG.5.1;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Thr19Ile" -e "p.Leu24del" -e "p.Pro25del" -e "p.Pro26del" -e "p.Ala27Ser" -e "p.Gln52His" -e "p.Val83Ala" -e "p.Gly142Asp" -e "p.Tyr144del" -e "p.His146Gln" -e "p.Gln183Glu" -e "p.Val213Glu" -e "p.Gly252Val" -e "p.Gly339His" -e "p.Arg346Thr" -e "p.Leu368Ile" -e "p.Ser371Phe" -e "p.Ser373Pro" -e "p.Ser375Phe" -e "p.Thr376Ala" -e "p.Arg403Lys" -e "p.Asp405Asn" -e "p.Arg408Ser" -e "p.Lys417Asn" -e "p.Asn440Lys" -e "p.Val445Pro" -e "p.Gly446Ser" -e "p.Asn450Asp" -e "p.Leu452Trp" -e "p.Leu455Phe" -e "p.Phe456Leu" -e "p.Asn460Lys" -e "p.Ser477Asn" -e "p.Thr478Lys" -e "p.Glu484Ala" -e "p.Phe486Ser" -e "p.Phe490Ser" -e "p.Arg493Gln" -e "p.Gln498Arg" -e "p.Asn501Tyr" -e "p.Tyr505His" -e "p.Asp614Gly" -e "p.His655Tyr" -e "p.Asn679Lys" -e "p.Pro681His" -e "p.Asn764Lys" -e "p.Asp796Tyr" -e "p.Gln954His" -e "p.Asn969Lys" $a > $a-variant-omicron-HK.3
done

echo "Detection of currently circulating omicron VOC - HK.3 sublineage (NextStrain 23H / PANGO HK.3). Exhaustive mutations on S protein: T19I, Δ24, Δ25, Δ26, A27S, Q52H, V83A, G142D, Δ144, H146Q, Q183E, V213E, G252V, G339H, R346T, L368I, S371F, S373P, S375F, T376A, R403K, D405N, R408S, K417N, N440K, V445P, G446S, N450D, L452W, L455F, F456L, N460K, S477N, T478K, E484A, F486S, F490S, R493Q, Q498R, N501Y, Y505H, D614G, H655Y, N679K, P681H, N764K, D796Y, Q954H, N969K" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-omicron-HK.3 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-omicron-HK.3;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Thr19Ile" -e "p.Arg21Thr" -e "p.Leu24del" -e "p.Pro25del" -e "p.Pro26del" -e "p.Ala27Ser" -e "p.Ser50Leu" -e "p.His69_Val70del" -e "p.Val127Phe" -e "p.Gly142Asp" -e "p.Tyr144del" -e "p.Phe157Ser" -e "p.Arg158Gly" -e "p.Asn211del" -e "p.Leu212Ile" -e "p.Val213Gly" -e "p.Leu216Phe" -e "p.His245Asn" -e "Ala264Asp" -e "p.Ile332Val" -e "p.Gly339His" -e "p.Lys356Thr" -e "p.Ser371Phe" -e "p.Ser373Pro" -e "p.Ser375Phe" -e "p.Thr376Ala" -e "p.Arg403Lys" -e "p.Asp405Asn" -e "p.Arg408Ser" -e "p.Lys417Asn" -e "p.Asn440Lys" -e "p.Val445His" -e "p.Gly446Ser" -e "p.Asn450Asp" -e "p.Leu452Trp" -e "p.Asn460Lys" -e "p.Ser477Asn" -e "p.Thr478Lys" -e "p.Asn481Lys" -e "p.Val483del" -e "p.Glu484Lys" -e "p.Phe486Pro" -e "p.Arg493Gln" -e "p.Gln498Arg" -e "p.Asn501Tyr" -e "p.Tyr505His" -e "p.Glu554Lys" -e "p.Ala570Val" -e "p.Asp614Gly" -e "p.Pro621Ser" -e "p.His655Tyr" -e "p.Asn679Lys" -e "p.Pro681His" -e "p.Asn764Lys" -e "p.Asp796Tyr" -e "p.Ser939Phe" -e "p.Gln954His" -e "p.Asn969Lys" -e "p.Pro1143Leu" $a > $a-variant-omicron-BA.2.86
done

echo "Detection of currently circulating omicron VOC - BA.2.86 sublineage (NextStrain 23I / PANGO BA.2.86). Exhaustive mutations on S protein: T19I, R21T, Δ24, Δ25, Δ26, A27S, S50L, Δ69-70, V127F, G142D ,Δ144, F157S, R158G, Δ211, L212I, V213G, L216F, H245N, A264D, I332V, G339H, K356T, S371F, S373P, S375F, T376A, R403K, D405N, R408S, K417N, N440K, V445H, G446S, N450D, L452W, N460K, S477N, T478K, N481K, Δ483, E484K, F486P, R493Q, Q498R, N501Y, Y505H, E554K, A570V, D614G, P621S, H655Y, N679K, P681R, N764K, D796Y, S939F, Q954H, N969K, P1143L" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-omicron-BA.2.86 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-omicron-BA.2.86;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Thr19Ile" -e "p.Arg21Thr" -e "p.Leu24del" -e "p.Pro25del" -e "p.Pro26del" -e "p.Ala27Ser" -e "p.Ser50Leu" -e "p.His69_Val70del" -e "p.Val127Phe" -e "p.Gly142Asp" -e "p.Tyr144del" -e "p.Phe157Ser" -e "p.Arg158Gly" -e "p.Asn211del" -e "p.Leu212Ile" -e "p.Val213Gly" -e "p.Leu216Phe" -e "p.His245Asn" -e "Ala264Asp" -e "p.Ile332Val" -e "p.Gly339His" -e "p.Lys356Thr" -e "p.Ser371Phe" -e "p.Ser373Pro" -e "p.Ser375Phe" -e "p.Thr376Ala" -e "p.Arg403Lys" -e "p.Asp405Asn" -e "p.Arg408Ser" -e "p.Lys417Asn" -e "p.Asn440Lys" -e "p.Val445His" -e "p.Gly446Ser" -e "p.Asn450Asp" -e "p.Leu452Trp" -e "p.Leu455Ser" -e "p.Asn460Lys" -e "p.Ser477Asn" -e "p.Thr478Lys" -e "p.Asn481Lys" -e "p.Val483del" -e "p.Glu484Lys" -e "p.Phe486Pro" -e "p.Arg493Gln" -e "p.Gln498Arg" -e "p.Asn501Tyr" -e "p.Tyr505His" -e "p.Glu554Lys" -e "p.Ala570Val" -e "p.Asp614Gly" -e "p.Pro621Ser" -e "p.His655Tyr" -e "p.Asn679Lys" -e "p.Pro681His" -e "p.Asn764Lys" -e "p.Asp796Tyr" -e "p.Ser939Phe" -e "p.Gln954His" -e "p.Asn969Lys" -e "p.Pro1143Leu" $a > $a-variant-omicron-JN.1
done

echo "Detection of currently circulating omicron VOC - JN.1 sublineage (NextStrain 24A / PANGO JN.1). Exhaustive mutations on S protein: T19I, R21T, Δ24, Δ25, Δ26, A27S, S50L, Δ69-70, V127F, G142D, Δ144, F157S, R158G, Δ211, L212I, V213G, L216F, H245N, A264D, I332V, G339H, K356T, S371F, S373P, S375F, T376A, R403K, D405N, R408S, K417N, N440K, V445H, G446S, N450D, L452W, L455S, N460K, S477N, T478K, N481K, Δ483, E484K, F486P, R493Q, Q498R, N501Y, Y505H, E554K, A570V, D614G, P621S, H655Y, N679K, P681R, N764K, D796Y, S939F, Q954H, N969K, P1143L" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-omicron-JN.1 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-omicron-JN.1;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Thr19Ile" -e "p.Arg21Thr" -e "p.Leu24del" -e "p.Pro25del" -e "p.Pro26del" -e "p.Ala27Ser" -e "p.Ser50Leu" -e "p.His69_Val70del" -e "p.Val127Phe" -e "p.Gly142Asp" -e "p.Tyr144del" -e "p.Phe157Ser" -e "p.Arg158Gly" -e "p.Asn211del" -e "p.Leu212Ile" -e "p.Val213Gly" -e "p.Leu216Phe" -e "p.His245Asn"-e "Ala264Asp" -e "p.Ile332Val" -e "p.Gly339His" -e "p.Lys356Thr" -e "p.Ser371Phe" -e "p.Ser373Pro" -e "p.Ser375Phe" -e "p.Thr376Ala" -e "p.Arg403Lys" -e "p.Asp405Asn" -e "p.Arg408Ser" -e "p.Lys417Asn" -e "p.Asn440Lys" -e "p.Val445His" -e "p.Gly446Ser" -e "p.Asn450Asp" -e "p.Leu452Trp" -e "p.Leu455Ser" -e "p.Phe456Leu" -e "p.Asn460Lys" -e "p.Ser477Asn" -e "p.Thr478Lys" -e "p.Asn481Lys" -e "p.Val483del" -e "p.Glu484Lys" -e "p.Phe486Pro" -e "p.Arg493Gln" -e "p.Gln498Arg" -e "p.Asn501Tyr" -e "p.Tyr505His" -e "p.Glu554Lys" -e "p.Ala570Val" -e "p.Asp614Gly" -e "p.Pro621Ser" -e "p.His655Tyr" -e "p.Asn679Lys" -e "p.Pro681His" -e "p.Asn764Lys" -e "p.Asp796Tyr" -e "p.Ser939Phe" -e "p.Gln954His" -e "p.Asn969Lys" -e "p.Val1104Leu" -e "p.Pro1143Leu" $a > $a-variant-omicron-JN.1.11.1
done

echo "Detection of currently circulating omicron VOC - JN.1.11.1 sublineage (NextStrain 24B / PANGO JN.1.11.1). Exhaustive mutations on S protein: T19I, R21T, Δ24, Δ25, Δ26, A27S, S50L, Δ69-70, V127F, G142D, Δ144, F157S, R158G, Δ211, L212I, V213G, L216F, H245N, A264D, I332V, G339H, K356T, S371F, S373P, S375F, T376A, R403K, D405N, R408S, K417N, N440K, V445H, G446S, N450D, L452W, L455S, F456L, N460K, S477N, T478K, N481K, Δ483, E484K, F486P, R493Q, Q498R, N501Y, Y505H, E554K, A570V, D614G, P621S, H655Y, N679K, P681R, N764K, D796Y, S939F, Q954H, N969K, V1104L, P1143L" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-omicron-JN.1.11.1 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-omicron-JN.1.11.1;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Thr19Ile" -e "p.Arg21Thr" -e "p.Leu24del" -e "p.Pro25del" -e "p.Pro26del" -e "p.Ala27Ser" -e "p.Ser50Leu" -e "p.His69_Val70del" -e "p.Val127Phe" -e "p.Gly142Asp" -e "p.Tyr144del" -e "p.Phe157Ser" -e "p.Arg158Gly" -e "p.Asn211del" -e "p.Leu212Ile" -e "p.Val213Gly" -e "p.Leu216Phe" -e "p.His245Asn" -e "Ala264Asp" -e "p.Ile332Val" -e "p.Gly339His" -e "p.Lys356Thr" -e "p.Ser371Phe" -e "p.Ser373Pro" -e "p.Ser375Phe" -e "p.Thr376Ala" -e "p.Asp405Asn" -e "p.Arg408Ser" -e "p.Lys417Asn" -e "p.Asn440Lys" -e "p.Val445His" -e "p.Gly446Ser" -e "p.Leu455Ser" -e "p.Phe456Leu" -e "p.Asn460Lys" -e "p.Ser477Asn" -e "p.Thr478Lys" -e "p.Asn481Lys" -e "p.Val483del" -e "p.Glu484Lys" -e "p.Phe486Pro" -e "p.Arg493Gln" -e "p.Gln498Arg" -e "p.Asn501Tyr" -e "p.Tyr505His" -e "p.Glu554Lys" -e "p.Ala570Val" -e "p.Asp614Gly" -e "p.Pro621Ser" -e "p.His655Tyr" -e "p.Asn679Lys" -e "p.Pro681His" -e "p.Asn764Lys" -e "p.Asp796Tyr" -e "p.Ser939Phe" -e "p.Gln954His" -e "p.Asn969Lys" -e "p.Val1104Leu" -e "p.Pro1143Leu" $a > $a-variant-omicron-KP.3
done

echo "Detection of currently circulating omicron VOC - KP.3 sublineage (NextStrain 24C / PANGO KP.3). Exhaustive mutations on S protein: T19I, R21T, Δ24, Δ25, Δ26, A27S, S50L, Δ69-70,V127F,G142D, Δ144, F157S, R158G, Δ211, L212I, V213G, L216F, H245N, A264D, I332V, G339H, K356T, S371F, S373P, S375F, T376A, D405N, R408S, K417N, N440K, V445H, G446S, L455S, F456L, N460K, S477N, T478K, N481K, Δ483, E484K, F486P, R493Q, Q498R, N501Y, Y505H, E554K, A570V, D614G, P621S, H655Y, N679K, P681R, N764K, D796Y, S939F, Q954H, N969K, V1104L, P1143L" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-omicron-KP.3 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-omicron-KP.3;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Leu452Arg" -e "p.Thr478Lys" -e "p.Pro681Arg" -e "p.Tyr145His" -e "p.Ala222Val" $a > $a-variant-indienAY4.2
done

echo "Detection of delta AY4.2 (delta plus) VOC (PANGO B.1.617.2). Some notable mutations on S protein: L452R, T478K, P681R, Y145H, A222V" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-indienAY4.2 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-indienAY4.2;
###
###

for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Leu452Arg" $a > $a-variant-USA
done

echo "Detection of epsilon VOI (PANGO B.1.427/B.1.429). Some notable mutation on S protein: L452R" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-USA ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-USA;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Glu484Lys" $a > $a-variant-bresilien2
done

echo "Detection of zeta VOI (PANGO P.2). Some notable mutation on S protein: E484K" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-bresilien2 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-bresilien2;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference"  -e "p.Gln52Arg" -e "p.Ala67Val" -e "p.His69_Val70del" -e "p.Tyr144del" -e "p.Glu484Lys" -e "p.Asp614Gly" -e "p.Gln677His" -e "p.Phe888Leu" $a > $a-variant-multi
done

echo "Detection of eta VOI (NextStrain 21D / PANGO B.1.525). Exhaustive mutations on S protein: Q52R, A67V, Δ69-70, Δ144, E484K, D614G, Q677H, F888L" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-multi ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-multi;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Glu484Lys" -e "p.Asn501Tyr" -e "p.Asp614Gly" -e "p.Pro681His" -e "p.Glu1092Lys" -e "p.His1101Tyr" -e "p.Val1176Phe" $a > $a-variant-philippines
done

echo "Detection of theta VOI (PANGO P.3). Some notable mutations on S protein: E484K, N501Y, D614G, P681H, E1092K, H1101Y, V1176F" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-philippines ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-philippines;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Glu484Lys" $a > $a-variant-USA2
done

echo "Detection of iota VOI (PANGO B.1.526). Some notable mutation on S protein: E484K" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1



for e in ${VCF}/*variant-USA2 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-USA2;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Glu154Lys" -e "p.Leu452Arg" -e "p.Glu484Gln" -e "p.Asp614Gly" -e "p.Pro681Arg" -e "p.Gln1071His" $a > $a-variant-indien2
done

echo "Detection of kappa VOI (NextStrain 21B / PANGO B.1.617.1). Exhaustive mutations on S protein: E154K, L452R, E484Q, D614G, P681R, Q1071H" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


for e in ${VCF}/*variant-indien2 ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-indien2;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Gly75Val" -e "p.Thr76Ile" -e "p.Arg246_Ser247_Tyr248_Leu249_Thr250_Pro251_Gly252del" -e "p.Asp253Asn" -e "p.Leu452Gln" -e "p.Phe490Ser" -e "p.Asp614Gly" -e "p.Thr859Asn" $a > $a-variant-perou
done

echo "Detection of lambda VOI (NextStrain 21G / PANGO C.37). Exhaustive mutations on S protein: G75V, T76I, Δ246-252, D253N, L452Q, F490S, D614G, T859N" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


for e in ${VCF}/*variant-perou ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-perou;
###
###


for a in ${VCF}/*pass_simple.xls ;
do
grep -F -e "reference" -e "p.Thr95Ile" -e "p.Asp109Asn" -e "p.Tyr144Ser" -e "p.Tyr145Asn" -e "p.Arg346Lys" -e "p.Glu484Lys" -e "p.Asn501Tyr" -e "p.Asp614Gly" -e "p.Pro681His" $a > $a-variant-colombie
done

echo "Detection of mu VOI (PANGO B.1.621). Some notable mutations on S protein: T95I, D109N, Y144S, Y145N, R346K, E484K, N501Y, D614G and P681H" >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


for e in ${VCF}/*variant-colombie ;
do
echo $e >> ${LOGFILE2} 2>&1;
    if [ -s $e ]
    then
        cat $e >> ${LOGFILE2} 2>&1;
    else
        echo "--> mutations not found" >> ${LOGFILE2} 2>&1;
    fi
done;

echo ""   >> ${LOGFILE2} 2>&1
echo "----------------------------------------------------------------------------------------" >> ${LOGFILE2} 2>&1


rm ${VCF}/*variant-colombie;
###

echo $CMD > $LOGCMD
eval $CMD 
