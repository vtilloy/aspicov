#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Variant filtering (apply filter + stats)               ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
FILTEREDVCFFILE=${args[0]}
PREFIX=${args[1]}
LOGCMD=${args[2]}


#get filter pass file
    CMD="grep -P '\tPASS\t|#' ${FILTEREDVCFFILE} > ${PREFIX}-annot-filter-pass.vcf"
    echo $CMD > $LOGCMD
    eval $CMD


#total
    CMD="grep -P '\tPASS\t' ${PREFIX}-annot-filter-pass.vcf > ${PREFIX}-annot-filter-total.vcf"
    echo $CMD >> $LOGCMD
    eval $CMD

    CMD="wc -l ${PREFIX}-annot-filter-total.vcf > ${PREFIX}-annot-filter-pass.vcf.total.txt"
    echo $CMD >> $LOGCMD
    eval $CMD


#stop loss
    CMD="grep -P 'stop_lost' ${PREFIX}-annot-filter-pass.vcf > ${PREFIX}-annot-filter-stop.vcf"
    echo $CMD >> $LOGCMD
    eval $CMD

    CMD="wc -l ${PREFIX}-annot-filter-stop.vcf > ${PREFIX}-annot-filter-stop.vcf.stop.txt"
    echo $CMD >> $LOGCMD
    eval $CMD


#missense variant
    CMD="grep -P 'missense_variant' ${PREFIX}-annot-filter-pass.vcf > ${PREFIX}-annot-filter-miss.vcf"
    echo $CMD >> $LOGCMD
    eval $CMD

    CMD="wc -l ${PREFIX}-annot-filter-miss.vcf > ${PREFIX}-annot-filter-miss.vcf.miss.txt"
    echo $CMD >> $LOGCMD
    eval $CMD
