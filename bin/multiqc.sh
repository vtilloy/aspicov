#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Sum Quality Reports                                    ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
LOGCMD=${args[0]}


# Run Multiqc
CMD="multiqc ."
    echo $CMD > $LOGCMD
    eval $CMD
