#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Quality check                                          ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
TECH=${args[0]}
PREFIX=${args[1]}
LOGCMD=${args[2]}
if [ $TECH == 'Illumina' ]
then
	READF=${args[3]}
	READR=${args[4]}

elif [ $TECH == 'Iontorrent' ]
then
	BAM=${args[3]}
fi


# Run Fastqc
if [ $TECH == 'Illumina' ]
then
    mkdir fastqc_${PREFIX}_logs
    CMD="fastqc -o fastqc_${PREFIX}_logs -f fastq -q ${READF} ${READR}"
    echo $CMD > $LOGCMD
    eval $CMD
elif [ $TECH == 'Iontorrent' ]
then
    mkdir fastqc_${PREFIX}_logs
    CMD="fastqc -o fastqc_${PREFIX}_logs  -f fastq -q ${BAM}"
    echo $CMD >> $LOGCMD
    eval $CMD
fi
