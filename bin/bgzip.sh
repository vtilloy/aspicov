#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: bgzip VCF for consensus and S gene extraction steps    ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
VCFPASS=${args[0]}
PREFIX=${args[1]}
LOGCMD=${args[2]}


# Run bgzip
    CMD="bgzip ${VCFPASS}"
    echo $CMD > $LOGCMD
    eval $CMD 
