#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Variant calling with Freebayes + normalization         ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
REF=${args[0]}
PREFIX=${args[1]}
BAMFILE=${args[2]}
LOGCMD=${args[3]}


# Run Freebayes
    CMD="freebayes -F 0.005  -q 17 -m20 -C 5 -f ${REF} -b ${BAMFILE} > ${PREFIX}_tmp.vcf"
    echo $CMD > $LOGCMD
    eval $CMD