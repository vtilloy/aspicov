#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: VCF indel normalization                                ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
REF=${args[0]}
PREFIX=${args[1]}
VCFFILE=${args[2]}
LOGCMD=${args[3]}


# Run normalization
    CMD="bcftools norm -f ${REF} -m -both -o ${PREFIX}-norm.vcf ${VCFFILE}"
    echo $CMD > $LOGCMD
    eval $CMD