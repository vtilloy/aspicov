#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Illumina adapters trimming                             ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
NCPUS=${args[0]}
READ_F=${args[1]}
READ_R=${args[2]}
PREFIX=${args[3]}
TRIMMOMATIC_ADAPTER_FILE=${args[4]}
LOGCMD=${args[5]}


# Run Trimmomatic
    CMD="trimmomatic PE -threads $NCPUS -phred33 $READ_F $READ_R ${PREFIX}_R1_trimmed.fastq.gz ${PREFIX}_R1_unpaired.fastq.gz ${PREFIX}_R2_trimmed.fastq.gz ${PREFIX}_R2_unpaired.fastq.gz  ILLUMINACLIP:${TRIMMOMATIC_ADAPTER_FILE}:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36"
    echo $CMD > $LOGCMD
    eval $CMD

