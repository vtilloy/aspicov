#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Alignment treatment and Mean depth                     ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
PREFIX=${args[0]}
SAMFILE=${args[1]}
REF=${args[2]}
BEDREF=${args[3]}
LOGCMD=${args[4]}


# Run samtools
    CMD="samtools view -bS ${SAMFILE} > ${PREFIX}_trimmed.bam"
    echo $CMD > $LOGCMD
    eval $CMD

#    CMD="samtools view -F4 -b ${PREFIX}_trimmed-all.bam > ${PREFIX}_trimmed.bam"
#    echo $CMD > $LOGCMD
#    eval $CMD

    CMD="samtools sort -T ${PREFIX}_trimmed.bam_sorted -o ${PREFIX}_trimmed.bam_sorted.bam ${PREFIX}_trimmed.bam"
    echo $CMD >> $LOGCMD
    eval $CMD

    CMD="samtools index ${PREFIX}_trimmed.bam_sorted.bam"
    echo $CMD >> $LOGCMD
    eval $CMD

    CMD="samtools idxstats ${PREFIX}_trimmed.bam_sorted.bam > ${PREFIX}_stats.csv"
    echo $CMD >> $LOGCMD
    eval $CMD
 
    samtools depth ${PREFIX}_trimmed.bam_sorted.bam | awk '{c++;s+=$3}END{print s/c}' > ${PREFIX}_trimmed.depth.txt
   
#    CMD=" samtools depth ${PREFIX}_trimmed.bam_sorted.bam | awk '{c++;s+=$3}END{print s/c}' > ${PREFIX}_trimmed.depth.txt"
#    echo $CMD >> $LOGCMD
#    eval $CMD

    CMD="samtools stats ${PREFIX}_trimmed.bam_sorted.bam -r ${REF} -t ${BEDREF} -g 10 > ${PREFIX}-10.txt"
    echo $CMD >> $LOGCMD
    eval $CMD

    CMD=" grep -w 'percentage of target genome with coverage' ${PREFIX}-10.txt > ${PREFIX}-10-stats.txt"
    echo $CMD >> $LOGCMD
    eval $CMD

# Extract regions less than 10X
    samtools depth -J -aa ${PREFIX}_trimmed.bam_sorted.bam | 
        awk '$3 < 10' > ${PREFIX}-tmp.bed 

    awk -F "\t" '{print $1 "\t" $2 "\t" $2 "\t" $3}' ${PREFIX}-tmp.bed > ${PREFIX}-less10Xregion.bed 

# Report files
    CMD="samtools stats ${PREFIX}_trimmed.bam_sorted.bam > ${PREFIX}-stats.tsv"
    echo $CMD >> $LOGCMD
    eval $CMD

    CMD="samtools idxstats ${PREFIX}_trimmed.bam_sorted.bam > ${PREFIX}-idxstats.tsv"
    echo $CMD >> $LOGCMD
    eval $CMD

    CMD="samtools flagstat ${PREFIX}_trimmed.bam_sorted.bam > ${PREFIX}-flagstats.tsv"
    echo $CMD >> $LOGCMD
    eval $CMD