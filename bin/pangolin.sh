#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Genotyping using Pangolin                              ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
CONSENSUS=${args[0]}
LOGCMD=${args[1]}


# Put consensus files into single fasta
    CMD="cat ${CONSENSUS} >> all.fasta"
    echo $CMD > $LOGCMD
    eval $CMD

# Run Pangolin
    CMD="pangolin all.fasta --outfile pangolin-lineage.tsv"
    echo $CMD >> $LOGCMD
    eval $CMD

