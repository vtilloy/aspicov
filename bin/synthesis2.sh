#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: files synthesis 2                                      ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
SAMTOOLSDIRECTORY=${args[0]}
LOGCMD=${args[1]}

#synthesis
    CMD="more ${SAMTOOLSDIRECTORY}/*-10-stats.txt | cat > all.10-stats.txt;"
    echo $CMD > $LOGCMD
    eval $CMD