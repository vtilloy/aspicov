#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: files synthesis 1                                      ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
SAMTOOLSDIRECTORY=${args[0]}
LOGCMD=${args[1]}

#synthesis
    CMD="more ${SAMTOOLSDIRECTORY}/*.depth.txt | cat > all.depth.txt;"
    echo $CMD > $LOGCMD
    eval $CMD