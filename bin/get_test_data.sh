#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: get test data for testing ASPICov                      ##
##                                                                           ##
###############################################################################

args=("$@")
BASEDIR=${args[0]}
TEST_DATA_OK=${args[1]}

datadir="$BASEDIR/test_dataset"

if [ ! -d "$datadir" ] || ([ -d "$datadir" ] && [ ! "$(ls -A $datadir)" ])
then 
     mkdir -p $datadir
     wget -r -nc -l1 -nH --cut-dirs=6  ftp://ftp.ifremer.fr/ifremer/dataref/bioinfo/sebimer/sequence-set/AspiCov/test_data/ -P $datadir
fi

   ln -s $datadir/test_data/*.gz .
   ln -s $datadir/test_data/*.bam .
   ln -s $datadir/test_data/*.genome .
   ln -s $datadir/test_data/*.fasta .
   ln -s $datadir/test_data/*.fa .
   ln -s $datadir/test_data/*.bedpe .
   ln -s $datadir/test_data/*.bed .

touch $TEST_DATA_OK