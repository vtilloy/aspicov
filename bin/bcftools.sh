#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Variant filtering (set filter) + VAF calculation       ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
VCFFILE=${args[0]}
PREFIX=${args[1]}
LOGCMD=${args[2]}


# Calculate VAF
    CMD="bcftools +fill-tags ${VCFFILE} -Ov -o ${VCFFILE}-vaf.vcf -- -t FORMAT/VAF"
    echo $CMD > $LOGCMD
    eval $CMD

# Sort vcf
#    CMD="bcftools sort ${VCFFILE}-vaf-tmp.vcf -Ov -o ${VCFFILE}-vaf.vcf"    
#    echo $CMD >> $LOGCMD
#    eval $CMD


# Set filters
    CMD="bcftools filter -s LowQual -e '%QUAL<200 || INFO/DP<100 || FORMAT/VAF<0.02' ${VCFFILE}-vaf.vcf > ${PREFIX}-annot-filter.vcf"
#    CMD="bcftools filter -s LowQual -e 'INFO/DP<100 || FORMAT/VAF<0.02' ${VCFFILE}-vaf.vcf > ${PREFIX}-annot-filter.vcf"
    echo $CMD >> $LOGCMD
    eval $CMD


# Report files
    CMD="bcftools stats ${PREFIX}-annot-filter.vcf > ${PREFIX}-bcftools-stats.txt"
    echo $CMD >> $LOGCMD
    eval $CMD