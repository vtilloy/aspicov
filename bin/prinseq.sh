#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: trimming low size (min 50)                             ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
FASTQFILE=${args[0]}
PREFIX=${args[1]}
LOGCMD=${args[2]}


# Run prinseq
    CMD="prinseq-lite.pl -verbose -fastq ${FASTQFILE} -min_len 50 -out_good stdout -out_bad null > ${PREFIX}_trimmed.fastq"
    echo $CMD > $LOGCMD
    eval $CMD

