#!/usr/bin/env python3

import sys
import matplotlib.pyplot as plt

fileName= sys.argv[1]
Pos=[]
D=[]

with open(fileName,'r') as f:
    for line in f:
        if line[0] not in ['t','#','v']: 
            l=line.rstrip().split('\t') 
            Pos.append(int(l[0]))
            D.append(int(float(l[1]))+int(float(l[2]))+int(float(l[3]))+int(float(l[4]))+int(float(l[5]))) 
            
plt.figure(figsize=(15,4)) 
plt.plot(Pos,D) 
plt.xlabel('Position (bp)',fontsize=16) 
plt.ylabel('Sequencing depth',fontsize=16)
plt.plot([0, 30000], [0, 0], color='k', linestyle='-', linewidth=0.5)
plt.plot([0, 30000], [100, 100], color='r', linestyle=':', linewidth=0.5)
plt.plot([21563, 21563], [0, 6000], color='purple', linestyle='-', linewidth=1)
plt.plot([25384, 25384], [0, 6000], color='purple', linestyle='-', linewidth=1)
plt.text(21200, 7000, 'spike glycoprotein',fontsize=12, color='purple')
xmin=0;xmax=30000;ymin=0;ymax=6000
plt.savefig(fileName[:-4]+'_coverage.pdf',bbox_inches='tight')
plt.show()




