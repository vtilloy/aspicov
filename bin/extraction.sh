#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: S gene extraction                                      ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
CONSENSUSFILE=${args[0]}
PREFIX=${args[1]}
LOGCMD=${args[2]}


# Run S gene extraction
    CMD="touch NC_045512-S.gff3;
	echo -e  '##sequence-region\tNC_045512.2\t1\t29903' >> NC_045512-S.gff3 2>&1;
	echo -e '##species https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=2697049' >> NC_045512-S.gff3 2>&1;

	echo -e 'NC_045512.2\tRefSeq\tCDS\t21563\t25384\t.\t+\t0\tID=cds-YP_009724390.1;Parent=gene-GU280_gp02;Dbxref=Genbank:YP_009724390.1,GeneID:43740568;Name=YP_009724390.1;Note=structural protein%3B spike protein;gbkey=CDS;gene=S;locus_tag=GU280_gp02;product=surface glycoprotein;protein_id=YP_009724390.1' >> NC_045512-S.gff3 2>&1;

	bedtools getfasta -fi ${CONSENSUSFILE} -fo ${PREFIX}-S.fasta -bed NC_045512-S.gff3;"
    echo $CMD > $LOGCMD
    eval $CMD

# Rename S gene extraction according to file name    
    awk '/^>/ {gsub(/.fasta?$/,"",FILENAME);printf(">%s\n",FILENAME);next;} {print}' ${PREFIX}-S.fasta > ${PREFIX}-regionS.fasta


# Rename consensus according to file name    
    awk '/^>/ {gsub(/.fasta?$/,"",FILENAME);printf(">%s\n",FILENAME);next;} {print}' ${CONSENSUSFILE}  > ${PREFIX}-consensus.fasta

