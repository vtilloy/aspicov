#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import os
import fnmatch
import sys, getopt


def getPospVariants(vcf):
    pf=[]
    with open(vcf,'r') as f:
        for line in f:
            if line[0]!='#':
                l=line.rstrip().split('\t')
                if l[1].find('NC_045512.2')==-1:
                    pf.append((int(l[1]),100))
    return(pf)

def main(argv):
    path = ''
    outputfile = ''
 
    try:
       opts, args = getopt.getopt(argv,"hp:o:",["pfile=","ofile="])
    except getopt.GetoptError:
       print('plots-variants.py -p <path> -o <outputfile>')
       sys.exit(2)
    for opt, arg in opts:
       if opt == '-h':
          print('plots-variants.py -p <path> -o <outputfile>')
          sys.exit()
       elif opt in ("-p", "--pfile"):
          path = arg
       elif opt in ("-o", "--ofile"):
          outputfile = arg
    
   

    L=30000
    plt.figure(num=None, figsize=(19, 5), dpi=300)
    ax = plt.axes(frameon=False)


    i=0
    
    annotfile = fnmatch.filter(os.listdir(path), "*filter-pass.vcf")
   
    for annot in annotfile:
        var=(0.5+i*1.5)*np.ones(L)
        pf=getPospVariants(annot)
        for (x,y) in pf:
            var[x]=0.5+i*1.5+y/100
        plt.plot(np.arange(0,L),var,color="k")
        ax.text(L, 0.7+i*1.5, annot, fontsize=14)
        i+=1
    
    
    ax.set_frame_on(False)
    ax.get_xaxis().tick_bottom()
    ax.axes.get_yaxis().set_visible(False)
    xmin, xmax = ax.get_xaxis().get_view_interval()
    ymin, ymax = ax.get_yaxis().get_view_interval()
    xm=0
    plt.savefig(outputfile,bbox_inches='tight')
    plt.show()



if __name__ == "__main__":
   main(sys.argv[1:])
