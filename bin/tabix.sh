#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Consensus and S gene extraction                        ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
GZVCFFILE=${args[0]}
PREFIX=${args[1]}
LOGCMD=${args[2]}


# Run tabix
    CMD="tabix -p vcf ${GZVCFFILE}"
    echo $CMD > $LOGCMD
    eval $CMD
