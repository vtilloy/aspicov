#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Indexation of the reference genome                     ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
REF=${args[0]}
LOGCMD=${args[1]}


# Run samtools
    CMD="samtools faidx ${REF}"
    echo $CMD >> $LOGCMD
    eval $CMD

