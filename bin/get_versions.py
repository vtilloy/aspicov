#!/usr/bin/env python3
from collections import OrderedDict
import re

regexes = {
    'Pipeline ASPICov': ['aspicov-v.txt', r"(\S+)"],
    'Nextflow': ['nextflow-v.txt', r"(\S+)"],
    'Singularity': ['singularity-v.txt', r"(\S+)"],  
    'Bamtools': ['bamtools-v.txt', r"bamtools/(\S+)"],
    'Prinseq': ['prinseq-v.txt', r"prinseq/(\S+)"],
    'Trimmomatic': ['trimmomatic-v.txt', r"trimmomatic/(\S+)"],
    'FastQC': ['fastqc-v.txt', r"fastqc/(\S+)"],
    'MultiQC': ['multiqc-v.txt', r"multiqc/(\S+)"],
    'Bwa': ['bwa-v.txt', r"bwa/(\S+)"],
    'Samtools': ['samtools-v.txt', r"samtools/(\S+)"],
    'igvtools': ['igvtools-v.txt', r"igvtools/(\S+)"],
    'matplotlib': ['matplotlib-v.txt', r"matplotlib/(\S+)"],  
    'Freebayes': ['freebayes-v.txt', r"freebayes/(\S+)"],
    'vcflib': ['vcflib-v.txt', r"vcflib/(\S+)"],
    'snpeff': ['snpeff-v.txt', r"snpeff/(\S+)"],
    'snpsift': ['snpsift-v.txt', r"snpsift/(\S+)"],
    'Bcftools': ['bcftools-v.txt', r"bcftools/(\S+)"],
    'Bedtools': ['bedtools-v.txt', r"bedtools/(\S+)"],
    'tabix': ['tabix-v.txt', r"tabix/(\S+)"],
    'Megahit': ['megahit-v.txt', r"megahit/(\S+)"],
    'Quast': ['quast-v.txt', r"quast/(\S+)"],
    'Pangolin': ['pangolin-v.txt', r"pangolin/(\S+)"],
}

results = OrderedDict()
results['Pipeline ASPICov'] = '<span style="color:#3333ff;\">N/A</span>'
results['Nextflow'] = '<span style="color:#ff3333;\">N/A</span>'
results['Singularity'] = '<span style="color:#ff3333;\">N/A</span>'
results['Bamtools'] = '<span style="color:#999999;\">N/A</span>'
results['Prinseq'] = '<span style="color:#999999;\">N/A</span>'
results['Trimmomatic'] = '<span style="color:#999999;\">N/A</span>'
results['FastQC'] = '<span style="color:#999999;\">N/A</span>'
results['MultiQC'] = '<span style="color:#999999;\">N/A</span>'
results['Bwa'] = '<span style="color:#999999;\">N/A</span>'
results['Samtools'] = '<span style="color:#999999;\">N/A</span>'
results['igvtools'] = '<span style="color:#999999;\">N/A</span>'
results['matplotlib'] = '<span style="color:#999999;\">N/A</span>'
results['Freebayes'] = '<span style="color:#999999;\">N/A</span>'
results['vcflib'] = '<span style="color:#999999;\">N/A</span>'
results['snpeff'] = '<span style="color:#999999;\">N/A</span>'
results['snpsift'] = '<span style="color:#999999;\">N/A</span>'
results['Bcftools'] = '<span style="color:#999999;\">N/A</span>'
results['Bedtools'] = '<span style="color:#999999;\">N/A</span>'
results['tabix'] = '<span style="color:#999999;\">N/A</span>'
results['Megahit'] = '<span style="color:#999999;\">N/A</span>'
results['Quast'] = '<span style="color:#999999;\">N/A</span>'
results['Pangolin'] = '<span style="color:#999999;\">N/A</span>'
#ff3333
#3333ff

# Check if all keys in regexes exist in results
missing_keys = set(regexes.keys()) - set(results.keys())
if missing_keys:
    print(f"Warning: Missing keys in results: {', '.join(missing_keys)}")

# Check if all keys in results exist in regexes
extra_keys = set(results.keys()) - set(regexes.keys())
if extra_keys:
    print(f"Warning: Extra keys in results: {', '.join(extra_keys)}")

# Search each file using its regex
for k, v in regexes.items():
    try:
        with open(v[0]) as x:
            versions = x.read()
#            print(f"Content of {v[0]}:\n{versions}")  # print content of file for debbuging
            match = re.search(v[1], versions)
            if match:
                results[k] = "v{}".format(match.group(1))
    except FileNotFoundError:
        print(f"Warning: File not found for {k} ({v[0]})")
    except Exception as e:
        print(f"Error processing {k}: {e}")

# Remove empty keys (defining them above ensures correct order)
for k in ['Pipeline ASPICov', 'Nextflow', 'Singularity', 'Bamtools', 'Prinseq', 'Trimmomatic', 'FastQC', 'MultiQC', 'Bwa', 'Samtools', 'igvtools', 'matplotlib', 'Freebayes', 'vcflib', 'snpeff', 'snpsift', 'Bcftools', 'Bedtools', 'tabix', 'Megahit', 'Quast', 'Pangolin']:
    if results[k] == '<span style="color:#999999;\">N/A</span>':
        del(results[k])

# Dump to YAML
print ('''
id: 'software_versions_mqc'
section_name: 'Software Versions'
plot_type: 'html'
description: 'Summary of software versions used at the time of analysis.'
data: |
    <dl class="dl-horizontal">
''')
for k,v in results.items():
    print("        <dt>{}</dt><dd>{}</dd>".format(k,v))
print ("    </dl>")