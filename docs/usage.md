# ASPICov: Usage

## Table of contents
* [Introduction](#introduction)
* [Install the pipeline](#install-the-pipeline)
  * [Local installation](#local-installation)
  * [Adding your own system config](#your-own-config)
* [Running the pipeline](#running-the-pipeline)
  * [Updating the pipeline](#updating-the-pipeline)
  * [Reproducibility](#reproducibility)
  * [Main arguments](#main-arguments)
* [Mandatory arguments](#mandatory-arguments)
  * [`-profile`](#-profile)
  * [`--fastq`](#--fastq)
  * [`--bam`](#--bam)
  * [`--ref_gen`](#--ref_gen)
  * [`--ref_fa`](#--ref_fa)
  * [`--technology`](#--technology)
  * [`--method`](#--method)
* [Generic arguments](#generic-arguments)
  * [`--projectName`](#--projectName)
* [Trimmomatic](#trimmomatic)
  * [`--adapter_file`](#--adapter_file)
* [Job resources](#job-resources)
* [Other command line parameters](#other-command-line-parameters)

## Introduction

Nextflow handles job submissions on SLURM or other environments, and supervises running the jobs. Thus the Nextflow process must run until the pipeline is finished. We recommend that you put the process running in the background through `screen` / `tmux` or similar tool. Alternatively you can run nextflow within a cluster job submitted your job scheduler.

It is recommended to limit the Nextflow Java virtual machines memory. We recommend adding the following line to your environment (typically in `~/.bashrc` or `~./bash_profile`):

```bash
NXF_OPTS='-Xms1g -Xmx4g'
```

## Install the pipeline

### Local installation

Make sure that [Nextflow](https://www.nextflow.io/) [Singularity](https://www.sylabs.io/guides/3.0/user-guide/) is installed on your system, allowing full reproducibility.

How to install ASPICov:

```bash
git clone https://gitlab.com/vtilloy/aspicov.git
```


### Adding your own system config

To use this workflow on a computing cluster, it is necessary to provide a configuration file for your system. For some institutes, this one already exists and is referenced on [nf-core/configs](https://github.com/nf-core/configs#documentation). If so, you can simply download your institute custom config file and simply use `-c <institute_config_file>` in the launch command.

If your institute does not have a referenced config file, you can create it using files from [other infrastructure](https://github.com/nf-core/configs/tree/master/docs)

## Running the pipeline

The most simple command for running the pipeline is as follows:

```bash
nextflow run main.nf -profile test,singularity
```

This will launch the pipeline with the `test` configuration profile using `singularity`. See below for more information about profiles.

Note that the pipeline will create the following files in your working directory:

```bash
test_dataset    # Directory containing data necessary for test execution
work            # Directory containing the nextflow working files
aspicov_test    # Finished results (projectName, configurable, see below)
.nextflow_log   # Log file from Nextflow
# Other nextflow hidden files, eg. history of pipeline runs and old logs.
```
### Updating the pipeline

When you run the above command, Nextflow automatically runs the pipeline code from your git clone - even if the pipeline has been updated since. To make sure that you're running the latest version of the pipeline, make sure that you regularly update the version of the pipeline:

```bash
cd aspicov
git pull
```

### Reproducibility

It's a good idea to specify a pipeline version when running the pipeline on your data. This ensures that a specific version of the pipeline code and software are used when you run your pipeline. If you keep using the same tag, you'll be running the same version of the pipeline, even if there have been changes to the code since.

First, go to the [ASPICov releases page](https://gitlab.com/vtilloy/aspicov/-/releases) and find the latest version number (eg. `v1.2.0`). Then, you can configure your local aspicov installation to use your desired version as follows:

```bash
cd aspicov
git checkout v1.2.0
```


## Mandatory arguments

### `-profile`

Use this parameter to choose a configuration profile. Profiles can give configuration presets for different compute environments. Note that multiple profiles can be loaded, for example: `-profile test,singularity`.

If `-profile` is not specified at all the pipeline will be run locally and expects all software to be installed and available on the `PATH`.

* `singularity`
  * A generic configuration profile to be used with [Singularity](http://singularity.lbl.gov/)

Profiles are also available to configure the workflow and can be combined with execution profiles listed above.

* `test`
  * A profile with a complete configuration for automated testing of annotation workflow
  * Includes test dataset so needs no other parameters
* `custom`
  * A profile to complete according to your dataset and experiment

### `--fastq`

Path to input reads (fastq) to process.

### `--bam`

Path to input bam files to process.

### `--ref_gen`

Set the path to the .genome reference.

### `--ref_fa`

Set the path to the .fasta reference.


### `--technology`

 Indicate the type of technology used for the sequencing. Can be set to "Illumina" or "Iontorrent".

### `--method`

Indicate the type of approach used for the sequencing. Can be set to "Ampliseq" or "Capture".

## Generic arguments

### `--projectName`

Indicate the Analyzed project name.

## Trimmomatic

### `--adapter_file`

Set the path to the trimmomatic adapter file.

## BAMClipper

### `--bedpe_file`

Set the path to the bedpe file.
Note: you can find a script (utils/bed2bedpe.py) to convert your bed into bedpe file, usable by BAMClipper


## Job resources

Each step in the pipeline has a default set of requirements for number of CPUs, memory and time. For most of the steps in the pipeline, if the job exits with an error code of `143` (exceeded requested resources) it will automatically resubmit with higher requests (2 x original, then 3 x original). If it still fails after three times then the pipeline is stopped.

# Other command line parameters

### `--outdir`

The output directory where the results will be published.

### `-w/--work-dir`

The temporary directory where intermediate data will be written.

### `--email`

Set this parameter to your e-mail address to get a summary e-mail with details of the run sent to you when the workflow exits.

### `--email_on_fail`

Same as --email, except only send mail if the workflow is not successful.

### `-name`

Name for the pipeline run. If not specified, Nextflow will automatically generate a random mnemonic.

### `-resume`

Specify this when restarting a pipeline. Nextflow will used cached results from any pipeline steps where the inputs are the same, continuing from where it got to previously.

You can also supply a run name to resume a specific run: `-resume [run-name]`. Use the `nextflow log` command to show previous run names.

**NB:** Single hyphen (core Nextflow option)

### `-c`

Specify the path to a specific config file (this is a core NextFlow command).

**NB:** Single hyphen (core Nextflow option)

Note - you can use this to override pipeline defaults.

### `--max_memory`

Use to set a top-limit for the default memory requirement for each process.
Should be a string in the format integer-unit. eg. `--max_memory '8.GB'`

### `--max_time`

Use to set a top-limit for the default time requirement for each process.
Should be a string in the format integer-unit. eg. `--max_time '2.h'`

### `--max_cpus`

Use to set a top-limit for the default CPU requirement for each process.
Should be a string in the format integer-unit. eg. `--max_cpus 1`

### `--plaintext_email`

Set to receive plain-text e-mails instead of HTML formatted.

### `--monochrome_logs`

Set to disable colourful command line output and live life in monochrome.






