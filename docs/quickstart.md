# ASPICov: Quickstart guide

## Input file requirements

### Fastq files

FASTQ format is a text-based format for storing both a biological sequence (usually nucleotide sequence) and its corresponding quality scores. Both the sequence letter and quality score are each encoded with a single ASCII character for brevity.

### Bam files

Binary Alignment Map (BAM) is the comprehensive raw data of genome sequencing; it consists of the lossless, compressed binary representation of the Sequence Alignment Map (SAM)-files.

## Process parameters

The `custom.config` file control each process and it's parameters, **so it's very important to fulfill this file very carefully**. Otherwise, it's can lead to computational errors or misinterpretation of biological results.

In this section, we will describe the most important parameters for each process.

```projectName```: the name of your project, **without space, tabulation or accented characters**.

```outdir```: the path to save your results.

```reads```: the full path to your fastq files. 

or

```bam```: the full path to your bam files.

```ref_genome```: the full path to your reference genome (.genome). 

```ref_fasta```: the full path to your reference genome (.fasta). 

```technology```: the type of technology used for sequencing ; "Illumina" or "Iontorrent".

```method```: the type of method used for sequencing ; "Ampliseq" or "Capture".

```trimmomatic_adapter_file```: the full path to your adapter file.
