# ASPICov: Output

This document describes the output produced by the workflow.

## Intermediate results

During the workflow, intermediate files are generated for each and every process.
You can find these files in **`results/[projectName]/02_intermediate_data`**.

## Final results

ASPICov guides you and simplifies the post-workflow analysis by grouping the most relevant results by category :

* Quality
* Alignment statistics
* Depth
* Coverage
* Variants
* Consensus and lineage
* Gene S

All of these files are available in **`results/[projectName]/03_final_results`**.

Mutations of SARS-Cov2 variants on S proteins used in svh.sh are listed in [sars-cov2-variants](https://gitlab.com/vtilloy/sars-cov2-variants)