process svh {
    tag "step9/10: special variant highlight"
    label 'svh'
    publishDir "${params.outdir}/${params.projectName}/${params.vcf_dirname}", mode: 'copy', pattern : '*.report.xls'
    publishDir "${params.outdir}/${params.projectName}/${params.svh_dirname}", mode: 'copy', pattern : '*.report.xls'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'svh.cmd', saveAs : { svh_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    path(vcf_simple_file)

    output:
    path("*.report.xls")
    path "svh.cmd", emit: svh_cmd

    script:
    """
    svh.sh . svh.cmd  >& svh.log 2>&1
    """
}

