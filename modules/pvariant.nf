process pvariant {
    tag "Plot Variant"
    label 'pvariant'
    publishDir "${params.outdir}/${params.projectName}/${params.vcf_dirname}", mode: 'copy', pattern : '*.pdf'
    publishDir "${params.outdir}/${params.projectName}/${params.pvariant_dirname}", mode: 'copy'

    input:
    path(vcf_annot_filter_pass_file_to_plot)

    output:
    path("*")


    script:
    """
    plots-variants.py -p . -o plot-variant.pdf >& pvariant.log 2>&1
    """
}
