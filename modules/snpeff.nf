process snpeff {
    tag "step6/10: variant annotation"
    label 'snpeff'
    publishDir "${params.outdir}/${params.projectName}/${params.snpeff_dirname}", mode: 'copy' , pattern : '*annot.vcf'
    publishDir "${params.outdir}/${params.projectName}/${params.reporthtml_dirname}", mode: 'copy' , pattern : '*.csv'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'snpeff.cmd', saveAs : { snpeff_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    tuple val(sample_id), path(vcf_file)

    output:
    tuple val(sample_id), path("*annot.vcf"), emit: vcf_annot_file
    path("*annot.vcf"), emit: plot_annot_file
    path("*.csv"), emit: snpeff_stats_file
    path "snpeff.cmd", emit: snpeff_cmd


    script:
    """
    snpeff.sh ${vcf_file} ${sample_id} snpeff.cmd  >& snpeff.log 2>&1
    """
}

