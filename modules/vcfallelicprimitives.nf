process vcfallelicprimitives {
    tag "step5/10: normalization"
    label 'vcfallelicprimitives'

    publishDir "${params.outdir}/${params.projectName}/${params.vcfallelicprimitives_dirname}", mode: 'copy', pattern : '*.vcf'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'vcfallelicprimitives.cmd', saveAs : { vcfallelicprimitives_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    tuple val(sample_id), path(vcf_norm_file)

    output:
    tuple val(sample_id), path("*.vcf"), emit: vcf_file
    path "vcfallelicprimitives.cmd", emit: vcfallelicprimitives_cmd


    script:
    """
    vcfallelicprimitives.sh ${vcf_norm_file} ${sample_id} vcfallelicprimitives.cmd  >& vcfallelicprimitives.log 2>&1
    """
}
