process bamtools {
    tag "generating fastq ..."
    label 'bamtools'
    publishDir "${params.outdir}/${params.projectName}/${params.bamtools_dirname}", mode: 'copy' , pattern : '*.fastq'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'bamtools.cmd', saveAs : { bamtools_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    tuple val(sample_id), path(bam_file)


    output:
    tuple val(sample_id), path("*.fastq"), emit: fastq_file
    path "bamtools.cmd", emit: bamtools_cmd
    

    script:
    """
    bamtools.sh ${bam_file}  ${sample_id}  bamtools.cmd  >& bamtools.log 2>&1
    """
}

