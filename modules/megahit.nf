process denovo {
    tag "denovo assembly"
    label 'megahit'
    publishDir "${params.outdir}/${params.projectName}/${params.denovo_dirname}", mode: 'copy' , pattern : '*.fasta'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'megahit.cmd', saveAs : { megahit_cmd -> "cmd/${task.process}_complete.sh" }
    

    input:
      tuple val(sample_id), path(trimmed_files)


    output:
        tuple val(sample_id), path("*.fasta"), emit: denovo_file
        path "megahit.cmd", emit: megahit_cmd


    script:

    if( params.technology == 'Illumina')
    """ 
    megahit.sh ${params.technology} ${sample_id} megahit.cmd ${trimmed_files.join(' ')} >& megahit.log 2>&1
    """
    
    else if( params.technology == 'Iontorrent')
    """
    megahit.sh ${params.technology} ${sample_id} megahit.cmd ${trimmed_files} >& megahit.log 2>&1
    """

}