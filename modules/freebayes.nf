process freebayes {
    tag "step5/10: variant calling with Freebayes"
    label 'freebayes'
    publishDir "${params.outdir}/${params.projectName}/${params.freebayes_dirname}", mode: 'copy', pattern : '*.vcf'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'freebayes.cmd', saveAs : { freebayes_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    tuple val(sample_id), path(bam_ch)

    output:
    tuple val(sample_id), path("*_tmp.vcf"), emit: vcf_tmp_file
    path "freebayes.cmd", emit: freebayes_cmd


    script:
    """
    freebayes.sh ${params.ref_fasta}  ${sample_id} ${bam_ch}  freebayes.cmd  >& freebayes.log 2>&1
    """
}
