process index {
    tag "step4/10: reference indexation"
    label 'samtools'


    input:
    path(ref_fasta)


    output:
    path("*.fai"), emit: ref_index

    path "index.cmd", emit: index_cmd


    script:
    """
    index.sh ${ref_fasta}  index.cmd  >& index.log 2>&1
    """
}

