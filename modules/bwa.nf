process bwa {
    tag "step3/10: bwa mapping"
    label 'bwa'
    publishDir "${params.outdir}/${params.projectName}/${params.bwa_dirname}", mode: 'copy', pattern :'*.sam'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'bwa.cmd', saveAs : { bwa_cmd -> "cmd/${task.process}_complete.sh" }


    input:
      tuple val(sample_id), path(trimmed_files)
    

    output:
      tuple val(sample_id), path("*trimmed.sam"), emit: sam_file
      path "bwa.cmd", emit: bwa_cmd


    script:
    """
    bwa.sh  ${params.ref_fasta} bwa.cmd ${trimmed_files} ${sample_id} >& bwa.log 2>&1
    """


}


process bwa2 {
    tag "step4/10: bwa2 mapping"
    label 'bwa2'
    publishDir "${params.outdir}/${params.projectName}/${params.bwa2_dirname}", mode: 'copy', pattern :'*.sam'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'bwa2.cmd', saveAs : { bwa2_cmd -> "cmd/${task.process}_complete.sh" }


    input:
      tuple val(sample_id), path(primerclipped_file)
    

    output:
      tuple val(sample_id), path("*trimmed.sam"), emit: sam2_file
      path "bwa2.cmd", emit: bwa2_cmd


    script:
    """
    bwa.sh  ${params.ref_fasta} bwa2.cmd ${primerclipped_file} ${sample_id} >& bwa2.log 2>&1
    """


}
