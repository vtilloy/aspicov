process synthesis1 {
    tag "depth synthesis"
    label 'synthesis1'
    publishDir "${params.outdir}/${params.projectName}/${params.depth_dirname}", mode: 'copy', pattern : 'all.depth.txt'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'synthesis1.cmd', saveAs : { synthesis1_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    path(depth_file)

    output:
    path("all.depth.txt"), emit: synthesis1_file 
    path "synthesis1.cmd", emit: synthesis1_cmd


    script:
    """
    synthesis1.sh . synthesis1.cmd >& synthesis1.log 2>&1
    """
}


process synthesis2 {
    tag "coverage synthesis"
    label 'synthesis2'
    publishDir "${params.outdir}/${params.projectName}/${params.wig_dirname}", mode: 'copy', pattern : 'all.10-stats.txt'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'synthesis2.cmd', saveAs : { synthesis2_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    path(stats_file)

    output:
    path("all.10-stats.txt"), emit: synthesis2_file 
    path "synthesis2.cmd", emit: synthesis2_cmd


    script:
    """
    synthesis2.sh . synthesis2.cmd >& synthesis2.log 2>&1
    """
}



process synthesis3 {
    tag "variants synthesis"
    label 'synthesis3'
    publishDir "${params.outdir}/${params.projectName}/${params.vcf_dirname}", mode: 'copy', pattern : 'all_total.txt'
    publishDir "${params.outdir}/${params.projectName}/${params.vcf_dirname}", mode: 'copy', pattern : 'all_stop.txt'
    publishDir "${params.outdir}/${params.projectName}/${params.vcf_dirname}", mode: 'copy', pattern : 'all_miss.txt'   
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'synthesis3.cmd', saveAs : { synthesis3_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    path(variant_files) 

    output:
    path("all_total.txt"), emit: synthesis3_1_file 
    path("all_stop.txt"), emit: synthesis3_2_file 
    path("all_miss.txt"), emit: synthesis3_3_file 
    path "synthesis3.cmd", emit: synthesis3_cmd


    script:
    """
    synthesis3.sh . synthesis3.cmd >& synthesis3.log 2>&1
    """
}

