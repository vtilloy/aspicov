process quast {
    tag "denovo assembly qc"
    label 'quast'
    publishDir "${params.outdir}/${params.projectName}/${params.denovo_dirname}", mode: 'copy' , pattern : '*.tsv'
    publishDir "${params.outdir}/${params.projectName}/${params.denovo_dirname}", mode: 'copy' , pattern : '*.pdf'
    publishDir "${params.outdir}/${params.projectName}/${params.reporthtml_dirname}", mode: 'copy' , pattern : '*.tsv'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'quast.cmd', saveAs : { quast_cmd -> "cmd/${task.process}_complete.sh" }
    

    input:
        tuple val(sample_id), path(denovo_file)


    output:
        path("*.tsv"), emit: quast_stats_file
        path("*.pdf"), emit: quast_pdf_file
        path "quast.cmd", emit: quast_cmd


    script:

        """
        quast.sh ${denovo_file} ${sample_id} quast.cmd >& quast.log 2>&1
        """

}