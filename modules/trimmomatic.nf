process trimmomatic {
    tag "step1/10: trimming Illumina adapters"
    label 'trimmomatic'
    publishDir "${params.outdir}/${params.projectName}/${params.trimmomatic_dirname}", mode: 'copy', pattern : '*trimmed.fastq.gz'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'trimmomatic.cmd', saveAs : { trimmomatic_cmd -> "cmd/${task.process}_complete.sh" }    

    input:
    tuple val(sample_id), path(reads)


    output:
    tuple val(sample_id), path("*trimmed.fastq.gz"), emit: trimmed_files
    path "trimmomatic.cmd", emit: trimmomatic_cmd


    script:
    """
    trimmomatic.sh ${task.cpus} ${reads[0]} ${reads[1]} ${sample_id} ${params.trimmomatic_adapter_file} trimmomatic.cmd >& trimmomatic.log 2>&1
    """
}
