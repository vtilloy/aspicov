process prinseq {
    tag "step1/10: trimming low size (min 50)"
    label 'prinseq'
    publishDir "${params.outdir}/${params.projectName}/${params.prinseq_dirname}", mode: 'copy' , pattern : '*_trimmed.fastq'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'prinseq.cmd', saveAs : { prinseq_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    tuple val(sample_id), path(fastq_file)

    output:
    tuple val(sample_id), path("*_trimmed.fastq"), emit: trimmed_files
    path "prinseq.cmd", emit: prinseq_cmd


    script:
    """
    prinseq.sh ${fastq_file}  ${sample_id}  prinseq.cmd  >& prinseq.log 2>&1
    """
}
