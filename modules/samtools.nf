process samtools {
    tag "step4/10: mean depth"
    label 'samtools'
    publishDir "${params.outdir}/${params.projectName}/${params.samtools_dirname}", mode: 'copy', pattern : '*-10-stats.txt'
    publishDir "${params.outdir}/${params.projectName}/${params.samtools_dirname}", mode: 'copy', pattern : '*.bam'
    publishDir "${params.outdir}/${params.projectName}/${params.csv_dirname}", mode: 'copy', pattern : '*_stats.csv'
    publishDir "${params.outdir}/${params.projectName}/${params.samtools_dirname}", mode: 'copy', pattern : '*_trimmed.depth.txt'
    publishDir "${params.outdir}/${params.projectName}/${params.depth_dirname}", mode: 'copy', pattern : '*-less10Xregion.bed'
    publishDir "${params.outdir}/${params.projectName}/${params.reporthtml_dirname}", mode: 'copy' , pattern : '*.tsv'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'samtools.cmd', saveAs : { samtools_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    tuple val(sample_id), path(sam_file)

  
    output:
    tuple val(sample_id), path("*_trimmed.bam_sorted.bam"), emit: bam_file
    path("*_stats.csv")
    path("*_trimmed.depth.txt"), emit: depth_file
    path("*-10-stats.txt"), emit: stats_file
    tuple val(sample_id), path("*-less10Xregion.bed"), emit: tenxbed_file
    path("*.tsv"), emit: samtools_stats_file
    val(sample_id), emit: sample_file

    path "samtools.cmd", emit: samtools_cmd


    script:
    """
    samtools.sh ${sample_id} ${sam_file} ${params.ref_fasta} ${params.ref_bed} samtools.cmd >& samtools.log 2>&1
    """
}

process samtools2 {
    tag "step4/10: samtools2"
    label 'samtools2'
    publishDir "${params.outdir}/${params.projectName}/${params.samtools_dirname}", mode: 'copy', pattern : '*-10-stats.txt'
    publishDir "${params.outdir}/${params.projectName}/${params.samtools_dirname}", mode: 'copy', pattern : '*.bam'
    publishDir "${params.outdir}/${params.projectName}/${params.csv_dirname}", mode: 'copy', pattern : '*_stats.csv'
    publishDir "${params.outdir}/${params.projectName}/${params.samtools_dirname}", mode: 'copy', pattern : '*_trimmed.depth.txt'
    publishDir "${params.outdir}/${params.projectName}/${params.depth_dirname}", mode: 'copy', pattern : '*-less10Xregion.bed'
    publishDir "${params.outdir}/${params.projectName}/${params.reporthtml_dirname}", mode: 'copy' , pattern : '*.tsv'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'samtools2.cmd', saveAs : { samtools2_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    tuple val(sample_id), path(sam2_file)

  
    output:
    tuple val(sample_id), path("*_trimmed.bam_sorted.bam"), emit: bam2_file
    path("*_stats.csv")
    path("*_trimmed.depth.txt"), emit: depth_file
    path("*-10-stats.txt"), emit: stats_file
    tuple val(sample_id), path("*-less10Xregion.bed"), emit: tenxbed_file
    path("*.tsv"), emit: samtools_stats_file
    val(sample_id), emit: sample_file

    path "samtools2.cmd", emit: samtools2_cmd


    script:
    """
    samtools.sh ${sample_id} ${sam2_file} ${params.ref_fasta} ${params.ref_bed} samtools2.cmd >& samtools2.log 2>&1
    """
}
