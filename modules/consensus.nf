process bgzip {
    tag "step8/10: consensus and S gene extraction"
    label 'bgzip'
    publishDir "${params.outdir}/${params.projectName}/${params.bgzip_dirname}", mode: 'copy', pattern: '*.vcf.gz'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'bgzip.cmd', saveAs : { bgzip_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    tuple val(sample_id), path(vcf_annot_filter_pass_file)

    output:
    tuple val(sample_id), path("*.vcf.gz"), emit: vcf_zip_file
    path "bgzip.cmd", emit: bzip_cmd


    script:
    """
    bgzip.sh ${vcf_annot_filter_pass_file}  ${sample_id}  bgzip.cmd  >& bgzip.log 2>&1
    """
}

process tabix {
    tag "step8/10: consensus and S gene extraction"
    label 'tabix'
    publishDir "${params.outdir}/${params.projectName}/${params.tabix_dirname}", mode: 'copy', pattern: '*.tbi'
    publishDir "${params.outdir}/${params.projectName}/${params.bgzip_dirname}", mode: 'copy', pattern: '*.tbi'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'tabix.cmd', saveAs : { tabix_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    tuple val(sample_id), path(vcf_zip_file)

    output:
    tuple val(sample_id), path("*.tbi"), emit: vcf_index
    path "tabix.cmd", emit: tabix_cmd
    
    
    script:
    """
    tabix.sh ${vcf_zip_file}  ${sample_id}  tabix.cmd  >& tabix.log 2>&1
    """
}

process consensus {
    tag "step8/10: consensus and S gene extraction"
    label 'consensus' 
    publishDir "${params.outdir}/${params.projectName}/${params.extraction_dirname}", mode: 'copy', pattern : '*nn-consensus.fasta'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'consensus.cmd', saveAs : { consensus_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    tuple val(sample_id), path(vcf_zip_file), path(vcf_index)

    output:
    tuple val(sample_id), path("*nn-consensus.fasta"), emit: consensus_file_nn
    path "consensus.cmd", emit: consensus_cmd
    

    script:
    """
    consensus.sh ${vcf_zip_file}  ${sample_id}  ${params.ref_fasta}  consensus.cmd  >& consensus.log 2>&1
    """
}

process extraction {
    tag "step8/10: consensus and S gene extraction"
    label 'extraction'

    publishDir "${params.outdir}/${params.projectName}/${params.geneS_dirname}", mode: 'copy', pattern : '*regionS.fasta'
    publishDir "${params.outdir}/${params.projectName}/${params.consensus_dirname}", mode: 'copy', pattern : '*consensus.fasta'
    publishDir "${params.outdir}/${params.projectName}/${params.extraction_dirname}", mode: 'copy', pattern : '*.gff3'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'extraction.cmd', saveAs : { extraction_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    tuple val(sample_id), path(consensus_file_nn)

    output:
    path("*consensus.fasta"), emit: consensus_file
    path("*"), emit: extraction_file 
    path "extraction.cmd", emit: extraction_cmd


    script:
    """
    extraction.sh  ${consensus_file_nn}  ${sample_id}  extraction.cmd  >& extraction.log 2>&1
    """
}