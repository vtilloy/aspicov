process snpsift {
    tag "step7/10: simple VCF"
    label 'snpsift'

    publishDir "${params.outdir}/${params.projectName}/${params.filters_dirname}", mode: 'copy', pattern : '*_fields.xls'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'snpsift.cmd', saveAs : { snpsift_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    tuple val(sample_id), path(vcf_annot_filter_pass_file)

    output:
    tuple val(sample_id), path("*_fields.xls"), emit: xls_fields_file
    path "snpsift.cmd", emit: snpsift_cmd


    script:
    """
    snpsift.sh ${vcf_annot_filter_pass_file}  ${sample_id}  snpsift.cmd  >& snpsift.log 2>&1
    """
}


process bcftools_extr {
    tag "step7/10: simple VCF"
    label 'bcftools_extr'

    publishDir "${params.outdir}/${params.projectName}/${params.vcf_dirname}", mode: 'copy', pattern : '*_pass_simple.xls'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'bcftools_extr.cmd', saveAs : { bcftools_extr_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    tuple val(sample_id), path(vcf_annot_filter_pass_file), path(xls_fields_file)
    

    output:
    path("*_pass_simple.xls"), emit: vcf_simple_file
    path "bcftools_extr.cmd", emit: bcftools_extr_cmd


    script:
    """
    bcftools_extr.sh ${vcf_annot_filter_pass_file}  ${sample_id}  ${xls_fields_file}  bcftools_extr.cmd  >& bcftools_extr.log 2>&1
    """
}

