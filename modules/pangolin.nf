process pangolin {
    tag "step8/10: consensus genotyping"
    label 'pangolin'
    publishDir "${params.outdir}/${params.projectName}/${params.consensus_dirname}", mode: 'copy' , pattern : 'all.fasta'
    publishDir "${params.outdir}/${params.projectName}/${params.consensus_dirname}", mode: 'copy' , pattern : '*.tsv'
    publishDir "${params.outdir}/${params.projectName}/${params.reporthtml_dirname}", mode: 'copy' , pattern : '*.tsv'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'pangolin.cmd', saveAs : { pangolin_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    path(consensus_file)

    output:
    path("*.tsv"), emit: genotyping_file
    path("all.fasta"), emit: all_fasta_file
    path "pangolin.cmd", emit: pangolin_cmd
    

    script:
    """
    pangolin.sh "${consensus_file}"  pangolin.cmd  >& pangolin.log 2>&1
    """
}