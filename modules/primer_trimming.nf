process primer_trimming {
    tag "step4/10: removing primers"
    label 'primer_trimming'
    publishDir "${params.outdir}/${params.projectName}/${params.primerclipp_dirname}", mode: 'copy', pattern :'*_clipped.fastq'
    publishDir "${params.outdir}/${params.projectName}/${params.primerclipp_dirname}", mode: 'copy', pattern :'*_trimstats.txt'
    publishDir "${params.outdir}/${params.projectName}/${params.wig_dirname}", mode: 'copy', pattern : '*_trimstats.txt'
    publishDir "${params.outdir}/${params.projectName}/${params.primerclipp_dirname}", mode: 'copy', pattern :'*_trimmed_sequences.fasta'

    input:
    tuple val(sample_id), path(samfile)


    output:
    tuple val(sample_id), path("*_clipped.fastq"), emit: primerclipped_file
    tuple val(sample_id), path("*_trimstats.txt"), emit: trimstats_file
    tuple val(sample_id), path("*_trimmed_sequences.fasta"), emit: sequencetotrim_file



    script:
    """
    trimming-primers.py ${params.bedpe_file} ${samfile} ${params.ref_fasta} ${sample_id} >& primer_trimming.log 2>&1
    """


}
