process normalization {
    tag "step5/10: indel normalization"
    label 'normalization'
    label 'process_medium'
    publishDir "${params.outdir}/${params.projectName}/${params.normalization_dirname}", mode: 'copy', pattern : '*-norm.vcf'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'normalization.cmd', saveAs : { normalization_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    tuple val(sample_id), path(vcf_tmp_file)


    output:
    tuple val(sample_id), path("*-norm.vcf"), emit: vcf_norm_file
    path "normalization.cmd", emit: normalization_cmd


    script:
    """
    normalization.sh  ${params.ref_fasta}  ${sample_id}  ${vcf_tmp_file} normalization.cmd  >& normalization.log 2>&1

    """
}
