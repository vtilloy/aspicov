process get_software_version {
    tag "step10/10: final report"
    label 'software_version'
    publishDir "${params.outdir}/${params.projectName}/${params.reporthtml_dirname}", mode: 'copy', pattern : '*.txt'
    publishDir "${params.outdir}/${params.projectName}/${params.reporthtml_dirname}", mode: 'copy', pattern : '*.yaml'
    publishDir "${params.outdir}/${params.projectName}/${params.reporthtml_dirname}", mode: 'copy', pattern : 'software_version.cmd', saveAs : { software_version_cmd -> "cmd/${task.process}_complete.sh" }

    output:
        path('software_versions_mqc.yaml'), emit : software_versions_yaml
        path("*.txt"), emit: version_file

    script:
        """
        echo "$workflow.manifest.version" &> aspicov-v.txt
        echo "$workflow.nextflow.version" &> nextflow-v.txt
        singularity --version &> singularity-int-v.txt
        sed 's/singularity-ce version //'  singularity-int-v.txt > singularity-v.txt
        software_versions.sh "$baseDir/nextflow.config" software_version.cmd >& software_version.log 2>&1
        """
}


process get_input_info {
    tag "step10/10: final report"
    label 'input_info'
    publishDir "${params.outdir}/${params.projectName}/${params.reporthtml_dirname}", mode: 'copy', pattern : 'input_files_mqc.yaml'

    output:
        path('input_files_mqc.yaml'), emit : input_files

    script:
        """
        echo "id: 'input_files_mqc'" >> input_files_mqc.yaml
        echo "section_name: 'Input Informations'" >> input_files_mqc.yaml
        echo "plot_type: 'html'" >> input_files_mqc.yaml
        echo "description: 'Summary of data and path used for the analysis.'" >> input_files_mqc.yaml
        echo "data: |" >> input_files_mqc.yaml
        echo "    <dl class=\"dl-horizontal\">" >> input_files_mqc.yaml
        echo "        <dt>User</dt><dd>${workflow.userName}</dd>" >> input_files_mqc.yaml
        echo "        <dt>Date</dt><dd>${params.date}</dd>" >> input_files_mqc.yaml       
        echo "        <dt>Command</dt><dd>${workflow.commandLine}</dd>" >> input_files_mqc.yaml
        echo "        <dt>Directory</dt><dd>${workflow.launchDir}</dd>" >> input_files_mqc.yaml
        echo "        <dt>Technology</dt><dd>${params.technology}</dd>" >> input_files_mqc.yaml
        echo "        <dt>Method</dt><dd>${params.method}</dd>" >> input_files_mqc.yaml
        echo "        <dt>Reference</dt><dd>${params.ref_fasta}</dd>" >> input_files_mqc.yaml
        echo "        <dt>Illumina fastq.gz files</dt><dd>${params.reads}</dd>" >> input_files_mqc.yaml
        echo "        <dt>Iontorrent ubam files</dt><dd>${params.bam}</dd>" >> input_files_mqc.yaml
        echo "        <dt>bed file</dt><dd>${params.ref_bed}</dd>" >> input_files_mqc.yaml
        echo "    </dl>" >> input_files_mqc.yaml
        """
}


process report_multiqc_config {
    tag "step10/10: final report"
    label 'multiqc_config'
    publishDir "${params.outdir}/${params.projectName}/${params.reporthtml_dirname}", mode: 'copy', pattern : 'multiqc_config.yaml'
    
    output:
        path("multiqc_config.yaml"), emit : multiQCconfig_file

    script:
        """
        touch multiqc_config.yaml

        echo "extra_fn_clean_exts:" >> multiqc_config.yaml
        echo "- _R1" >> multiqc_config.yaml
        echo "- _R2" >> multiqc_config.yaml
        echo "- -annot-filter" >> multiqc_config.yaml
        echo "- -stats" >> multiqc_config.yaml
        echo "- -nn-consensus" >> multiqc_config.yaml

        echo "custom_logo_title: 'ASPICov Workflow'" >> multiqc_config.yaml
        echo "custom_logo: '${baseDir}/assets/logo_chu.gif'" >> multiqc_config.yaml
        echo "subtitle: 'ASPICov Pipeline Report'" >> multiqc_config.yaml
        echo "intro_text: 'ASPICov is an Automated and Standardized Pipeline for Identification of SARS-Cov2 nucleotidic Variants.'" >> multiqc_config.yaml 
        echo "report_header_info:" >> multiqc_config.yaml 
        echo "    - Source code: 'https://gitlab.com/vtilloy/aspicov'" >> multiqc_config.yaml
        echo "    - Publication: 'https://pubmed.ncbi.nlm.nih.gov/35081137/'" >> multiqc_config.yaml    
        echo "    - E-mail contact: 'valentin.tilloy@unilim.fr'" >> multiqc_config.yaml

        echo "top_modules:" >> multiqc_config.yaml
        echo "  - 'custom_content'" >> multiqc_config.yaml
        echo "  - 'fastqc'" >> multiqc_config.yaml
        echo "  - 'samtools'" >> multiqc_config.yaml
        echo "  - 'snpeff'" >> multiqc_config.yaml
        echo "  - 'bcftools'" >> multiqc_config.yaml
        echo "  - 'pangolin'" >> multiqc_config.yaml
        echo "  - 'quast'" >> multiqc_config.yaml

        echo "custom_content:" >> multiqc_config.yaml
        echo "  order:" >> multiqc_config.yaml
        echo "    - input_files_mqc" >> multiqc_config.yaml
        echo "    - software_versions_mqc" >> multiqc_config.yaml

        echo "table_columns_visible:" >> multiqc_config.yaml
        echo "  FastQC:" >> multiqc_config.yaml
        echo "    total_sequences: True" >> multiqc_config.yaml
        echo "    percent_duplicates: True" >> multiqc_config.yaml
        echo "    percent_gc: False" >> multiqc_config.yaml
        echo "    avg_sequence_length: True" >> multiqc_config.yaml
        echo "    percent_fails: False" >> multiqc_config.yaml
        echo "  Samtools:" >> multiqc_config.yaml
        echo "    flagstat_total: False" >> multiqc_config.yaml
        echo "    mapped_passed: False" >> multiqc_config.yaml
        echo "    error_rate: False" >> multiqc_config.yaml
        echo "    non-primary_alignments: False" >> multiqc_config.yaml
        echo "    reads_mapped: False" >> multiqc_config.yaml
        echo "    reads_mapped_percent: True" >> multiqc_config.yaml
        echo "    reads_properly_paired_percent: False" >> multiqc_config.yaml
        echo "    reads_MQ0_percent: False" >> multiqc_config.yaml
        echo "    raw_total_sequences: False" >> multiqc_config.yaml
        echo "  SnpEff:" >> multiqc_config.yaml
        echo "    Change_rate: False" >> multiqc_config.yaml
        echo "    Ts_Tv_ratio: False" >> multiqc_config.yaml
        echo "    Number_of_variants_before_filter: False" >> multiqc_config.yaml
        echo "  Bcftools Stats:" >> multiqc_config.yaml
        echo "    number_of_records: False" >> multiqc_config.yaml
        echo "    number_of_SNPs: True" >> multiqc_config.yaml
        echo "    number_of_indels: True" >> multiqc_config.yaml
        echo "    tstv: False" >> multiqc_config.yaml
        echo "    number_of_MNPs: False" >> multiqc_config.yaml
    
        """
}



process html_report {
    tag "step10/10: final report"
    label 'html_report'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : '*.html'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'html_report.cmd', saveAs : { html_report_cmd -> "cmd/${task.process}_complete.sh" }

    input:
        path(multiQCconfig_file)
        path(input_files)
        path(software_versions_yaml)
        path('fastqc_file/*')
        path('samtools_stats_file/*')
        path('bcftools_stats_file/*')
        path('snpeff_stats_file/*')
        path('genotyping_file/*')
        path('quast_stats_file/*')

    output:
        path("*.html")

    script:
        """
        html_report.sh ${params.projectName} html_report.cmd >& html_report.log 2>&1
        """

}