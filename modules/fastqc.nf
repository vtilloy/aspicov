process fastqc {
    tag "step2/10: quality check"
    label 'fastqc'
    publishDir "${params.outdir}/${params.projectName}/${params.fastqc_dirname}", mode: 'copy', pattern : '*_logs'
    publishDir "${params.outdir}/${params.projectName}/${params.quality_dirname}", mode: 'copy', pattern : '*_logs'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'fastqc.cmd', saveAs : { fastqc_cmd -> "cmd/${task.process}_complete.sh" }
    

    input:
      tuple val(sample_id), path(trimmed_files)

    output:
      path "fastqc_${sample_id}_logs", emit : fastqc_file
      path "fastqc.cmd", emit: fastqc_cmd

    script:
    if( params.technology == 'Illumina')
    """ 
    fastqc.sh ${params.technology}  ${sample_id} fastqc.cmd ${trimmed_files.join(' ')} >& fastqc.log 2>&1
    """
    
    else if( params.technology == 'Iontorrent')
    """
    fastqc.sh  ${params.technology} ${sample_id} fastqc.cmd ${trimmed_files}  >& fastqc.log 2>&1
    """
}