process igvtools {
    tag "step4/10: mean depth"
    label 'igvtools'
    publishDir "${params.outdir}/${params.projectName}/${params.wig_dirname}", mode: 'copy', pattern : '*.wig'
    publishDir "${params.outdir}/${params.projectName}/${params.igvtools_dirname}", mode: 'copy', pattern : '*.wig'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'igvtools.cmd', saveAs : { igvtools_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    tuple val(sample_id), path(bam_ch)

    output:
    path("*.wig"), emit: wig_file
    path "igvtools.cmd", emit: igvtools_cmd


    script:
    """
    igvtools.sh ${sample_id}  ${bam_ch} ${params.ref_genome}  igvtools.cmd  >& igvtools.log 2>&1
    """
}
