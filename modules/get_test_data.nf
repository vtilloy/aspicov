process get_test_data {
    label 'internet_access'

    output:
    tuple val('SAMN17767551'), path("*R{1,2}_001.fastq.gz"), emit: read_pairs
    path("*.bam"), emit: bam_file
    path("*.genome"), emit: ref_genome
    path("*.fasta"), emit: ref_fasta
    path("*.bed"), emit: ref_bed
    path("test_data_ok"), emit: data_ok


    script:
    """
    get_test_data.sh ${baseDir} test_data_ok  >& get_test_data.log 2>&1
    """
}

