process bcftools {
    tag "step7/10: set VAF + variant filtering"
    label 'bcftools'
    publishDir "${params.outdir}/${params.projectName}/${params.vcf_dirname}", mode: 'copy', pattern : '*annot-filter.vcf'
    publishDir "${params.outdir}/${params.projectName}/${params.bcftools_dirname}", mode: 'copy' , pattern : '*annot-filter.vcf'
    publishDir "${params.outdir}/${params.projectName}/${params.reporthtml_dirname}", mode: 'copy' , pattern : '*.txt'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'bcftools.cmd', saveAs : { bcftools_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    tuple val(sample_id), path(vcf_file)

    output:
    tuple val(sample_id), path("*annot-filter.vcf"), emit: vcf_annot_filter_file
    path("*.txt"), emit: bcftools_stats_file
    path "bcftools.cmd", emit: bcftools_cmd


    script:
    """
    bcftools.sh ${vcf_file}  ${sample_id}  bcftools.cmd  >& bcftools.log 2>&1
    """
}
