process filter {
    tag "step7/10: apply filter on VCF files"
    label 'filter'
    publishDir "${params.outdir}/${params.projectName}/${params.vcf_dirname}", mode: 'copy', pattern : '*annot-filter-pass.vcf'
    publishDir "${params.outdir}/${params.projectName}/${params.filters_dirname}", mode: 'copy', pattern : '*annot-filter-pass.vcf'
    publishDir "${params.outdir}/${params.projectName}/${params.filters_dirname}", mode: 'copy', pattern : '*.txt'
    publishDir "${params.outdir}/${params.projectName}/${params.report_dirname}", mode: 'copy', pattern : 'filter.cmd', saveAs : { filter_cmd -> "cmd/${task.process}_complete.sh" }

    input:
    tuple val(sample_id), path(vcf_annot_filter_file)

    output:
    tuple val(sample_id), path("*annot-filter-pass.vcf"), emit: vcf_annot_filter_pass_file
    path("*annot-filter-pass.vcf"), emit: vcf_annot_filter_pass_file_to_plot
    path("*.txt"), emit: variant_files
    path "filter.cmd", emit: filter_cmd

    script:
    """
    filter.sh  ${vcf_annot_filter_file} ${sample_id} filter.cmd  >& filter.log 2>&1
    """
}

